install_project:
	cd front-end && yarn install

build_project:
	cd front-end && yarn build

start_project:
	cd front-end && yarn start

selenium-tests:
	python3 front-end/gui-tests/about-test.py
	python3 front-end/gui-tests/cities-test.py
	python3 front-end/gui-tests/events-test.py
	python3 front-end/gui-tests/restaurants-test.py
