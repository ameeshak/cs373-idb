from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import unittest
from selenium.webdriver.common.by import By

# Followed examples from here: https://selenium-python.readthedocs.io/waits.html
class CitiesTest(unittest.TestCase):
    def setUp(self):
        self.url = "https://develop.out-n-about.me/cities"
        service = Service(ChromeDriverManager().install())
        self.driver = webdriver.Chrome(service=service)
        self.timeout = 5
    
    def tearDown(self):
        self.driver.quit()

    # Test that the grid loads
    def testGrid(self):
        self.driver.get(self.url)
        try:
            WebDriverWait(self.driver, self.timeout).until(
                EC.presence_of_element_located((By.ID, "grid"))
            )
        except:
            self.fail("Grid not loading")

    # Test that the images in the grid load
    def testGridImages(self):
        self.driver.get(self.url)
        try:
            WebDriverWait(self.driver, self.timeout).until(
                EC.presence_of_element_located((By.ID, "city-image"))
            )
        except:
            self.fail("Images within grid not loading")

    # Test that sorting works
    def testSorting(self):
        self.driver.get(self.url + "?sort=name")
        try:
            WebDriverWait(self.driver, self.timeout).until(
                EC.presence_of_element_located((By.XPATH, "//*[contains(text(),'Aberdeen')]"))
            )
        except:
            self.fail("Name sorting not working")

    # Test that searching works
    def testSearching(self):
        self.driver.get(self.url + "?search=California")

        try:
            WebDriverWait(self.driver, self.timeout).until(
                EC.presence_of_element_located((By.XPATH, "//*[contains(text(),'Long Beach')]"))
            )
        except:
            self.fail("Searching not working")

    # Test that filtering works
    def testFiltering(self):
        self.driver.get(self.url + "?population=1000000-9000000")
        try:
            WebDriverWait(self.driver, self.timeout).until(
                EC.presence_of_element_located((By.XPATH, "//*[contains(text(),'Chicago')]"))
            )
        except:
            self.fail("Population filtering not working")