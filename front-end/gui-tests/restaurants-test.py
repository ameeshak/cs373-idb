from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import unittest
from selenium.webdriver.common.by import By

# Followed examples from here: https://selenium-python.readthedocs.io/waits.html
class RestaurantsTest(unittest.TestCase):
    def setUp(self):
        self.url = "https://develop.out-n-about.me/restaurants"
        service = Service(ChromeDriverManager().install())
        self.driver = webdriver.Chrome(service=service)
        self.driver.get(self.url)
        self.timeout = 5
    
    def tearDown(self):
        self.driver.quit()

    # Test that the grid loads
    def testGrid(self):
        try:
            WebDriverWait(self.driver, self.timeout).until(
                EC.presence_of_element_located((By.ID, "grid"))
            )
        except:
            self.fail("Grid not loading")

    # Test that the images in the grid load
    def testGridImages(self):
        try:
            WebDriverWait(self.driver, self.timeout).until(
                EC.presence_of_element_located((By.ID, "rest-image"))
            )
        except:
            self.fail("Images within grid not loading")

    # Test that sorting works
    def testSorting(self):
        self.driver.get(self.url + "?sort=state")
        try:
            WebDriverWait(self.driver, self.timeout).until(
                EC.presence_of_element_located((By.XPATH, "//*[contains(text(),'Everest Restaurant')]"))
            )
        except:
            self.fail("State sorting not working")

    # Test that searching works
    def testSearching(self):
        self.driver.get(self.url + "?search=river")

        try:
            WebDriverWait(self.driver, self.timeout).until(
                EC.presence_of_element_located((By.XPATH, "//*[contains(text(),'BARDA Detroit')]"))
            )
        except:
            self.fail("Search not working")

    # Test that filtering works
    def testFiltering(self):
        self.driver.get(self.url + "?type=Barbeque")
        try:
            WebDriverWait(self.driver, self.timeout).until(
                EC.presence_of_element_located((By.XPATH, "//*[contains(text(),'OMC Smokehouse')]"))
            )
        except:
            self.fail("Cuisine filtering not working")