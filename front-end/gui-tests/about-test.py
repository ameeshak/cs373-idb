from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import unittest
from selenium.webdriver.common.by import By

# Followed examples from here: https://selenium-python.readthedocs.io/waits.html
class AboutTest(unittest.TestCase):
    def setUp(self):
        self.url = "https://develop.out-n-about.me/about"
        service = Service(ChromeDriverManager().install())
        self.driver = webdriver.Chrome(service=service)
        self.driver.get(self.url)
        self.timeout = 5
    
    def tearDown(self):
        self.driver.quit()

    # Test that the member images load
    def testMemberImages(self):
        try:
            WebDriverWait(self.driver, self.timeout).until(
                EC.presence_of_element_located((By.CLASS_NAME, "card-img-top"))
            )
        except:
            self.fail("Member images not found")

    # Test that the tools images load
    def testToolsImages(self):
        try:
            WebDriverWait(self.driver, self.timeout).until(
                EC.presence_of_element_located((By.ID, "amazon-pic"))
            )
        except:
            self.fail("Tools images not found")

    # Test that the api images load
    def testApiImages(self):
        try:
            WebDriverWait(self.driver, self.timeout).until(
                EC.presence_of_element_located((By.ID, "roadgoat-pic"))
            )
        except:
            self.fail("Api images not found")