from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import unittest
from selenium.webdriver.common.by import By

# Followed examples from here: https://selenium-python.readthedocs.io/waits.html
class EventsTest(unittest.TestCase):
    def setUp(self):
        self.url = "https://develop.out-n-about.me/events"
        service = Service(ChromeDriverManager().install())
        self.driver = webdriver.Chrome(service=service)
        self.driver.get(self.url)
        self.timeout = 5
    
    def tearDown(self):
        self.driver.quit()

    # Test that the grid loads
    def testGrid(self):
        try:
            WebDriverWait(self.driver, self.timeout).until(
                EC.presence_of_element_located((By.ID, "grid"))
            )
        except:
            self.fail("Grid not loading")
    
    # Test that sorting works
    def testSorting(self):
        self.driver.get(self.url + "?sort=name")
        try:
            WebDriverWait(self.driver, self.timeout).until(
                EC.presence_of_element_located((By.XPATH, "//*[contains(text(),'2022 Jackson State Football Season')]"))
            )
        except:
            self.fail("Name sorting not working")

    # Test that searching works
    def testSearching(self):
        self.driver.get(self.url + "?search=Arkansas")

        try:
            WebDriverWait(self.driver, self.timeout).until(
                EC.presence_of_element_located((By.XPATH, "//*[contains(text(),'Styx')]"))
            )
        except:
            self.fail("Search not working")

    # Test that filtering works
    def testFiltering(self):
        self.driver.get(self.url + "?price=50-70")
        try:
            WebDriverWait(self.driver, self.timeout).until(
                EC.presence_of_element_located((By.XPATH, "//*[contains(text(),'Raphael')]"))
            )
        except:
            self.fail("Price filtering not working")