import { useMemo } from 'react';

export const usePagination = (
    totalCount: number,
    pageSize: number
  ) => {
    const paginationRange = useMemo(() => {
        const totalPages = Math.ceil(totalCount / pageSize)
        return Array.from({ length: totalPages}, (_, idx) => idx + 1);
    
    }, [totalCount, pageSize]);
  
    return paginationRange;
  };
