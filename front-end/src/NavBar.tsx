import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import { FormControl } from 'react-bootstrap'
import { useNavigate } from "react-router-dom";
import React, { useReducer, useState } from 'react';

export function NavBar() {
    const navigate = useNavigate()
    const [inputText, setInputText] = useState("")
    let inputHandler = (e:any) => {
        //convert input text to lower case
        var lowerCase = e.target.value.toLowerCase()
        setInputText(lowerCase)
      }
      const handleSubmit = (event:any) => {
        event.preventDefault()
        if (inputText.trim() != "") {
            navigate("/search?query="+inputText)
        }
        
      }
    return (
        <nav className="navbar navbar-expand-lg bg-light">
            <div className="container-fluid">
                <a
                    className="navbar-brand"
                    aria-current="page"
                    href="/">
                    Out N About
                </a>
                <button
                    className="navbar-toggler"
                    type="button"
                    data-bs-toggle="collapse"
                    data-bs-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent"
                    aria-expanded="false"
                    aria-label="Toggle navigation"
                >
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div
                    className="collapse navbar-collapse"
                    id="navbarSupportedContent"
                >
                    <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                        <li className="nav-item">
                            <a
                                className="nav-link active"
                                aria-current="page"
                                href="/"
                            >
                                Home
                            </a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="/cities">
                                Cities
                            </a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="/events">
                                Events
                            </a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="/restaurants">
                                Restaurants
                            </a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="/ourviz">
                                Our Visualizations
                            </a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="/theirviz">
                                Provider Visualizations
                            </a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="/about">
                                About
                            </a>
                        </li>
                        <Form className="d-flex">
                  <Form.Control
                    type="search"
                    placeholder="Search"
                    className="me-2"
                    aria-label="Search"
                    onSubmit={handleSubmit}
                    onChange={inputHandler}
                  />
                  <Button type="submit" onClick={handleSubmit} variant="outline-primary">Search</Button>
                  </Form>
                    </ul>
                </div>
            </div>
        </nav>
    )
}
