import { MemoryRouter } from "react-router-dom";
import { render, screen } from "@testing-library/react";
import Home from "../components/Home";

describe("Home page testing", () => {
    test("First slide of slideshow renders", async () => {
      render(
        <MemoryRouter>
          <Home />
        </MemoryRouter>
      );
      const elem = screen.getByText("Find Cities");
      expect(elem).toBeInTheDocument();
    });
});
