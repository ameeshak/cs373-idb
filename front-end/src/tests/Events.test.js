import { MemoryRouter, BrowserRouter } from "react-router-dom";
import { cleanup, render, screen } from "@testing-library/react";
import Events from "../components/events/EventModel";
import { EventExactFilterBar, EventRangeFilterBar } from "../components/events/EventsToolBar";

afterEach(cleanup);

describe("Events page testing", () => {
    test("Events page renders", async () => {
      render(
        <MemoryRouter>
          <Events />
        </MemoryRouter>
      );
      const elem = screen.getByText("Events");
      expect(elem).toBeInTheDocument();
    });
});

describe("Exact Filters Testing", () => {
  test("Ensure the Exact Filter for Events Model renders", async () => {
    render (
      <MemoryRouter>
        <EventExactFilterBar/>      
      </MemoryRouter>
    );

    expect(screen.getByText("Filter By State")).toBeInTheDocument();
    expect(screen.getByText("Filter By Event Type")).toBeInTheDocument();

  });
});

describe("Range Filters Testing", () => {
  test("Ensure the Range Filter for Events Model renders", async () => {
    render (
      <MemoryRouter>
      <EventRangeFilterBar/>
      </MemoryRouter>
    );

    expect(screen.getByText("Filter By Date")).toBeInTheDocument();
    expect(screen.getByText("Filter By Price")).toBeInTheDocument();
    expect(screen.getByText("Filter By Time")).toBeInTheDocument();

  });
});
