import { MemoryRouter } from "react-router-dom";
import { render, screen, cleanup } from "@testing-library/react";
import Cities from "../components/cities/CitiesModel";
import { ExactFilterBar, RangeFilterBar } from "../components/cities/CitiesToolBar";

afterEach(cleanup);

describe("Cities page testing", () => {
    test("Cities page renders", async () => {
      render(
        <MemoryRouter>
          <Cities />
        </MemoryRouter>
      );
      const elem = screen.getByText("Cities");
      expect(elem).toBeInTheDocument();
    });
});

describe("Range Filters Testing", () => {
  test("Ensure the Range Filter for Cities Model renders", async () => {
    render (
      <MemoryRouter>
      <RangeFilterBar/>
      </MemoryRouter>
    );

    expect(screen.getByText("Filter By Cost Of Living")).toBeInTheDocument();
    expect(screen.getByText("Filter By Average Rating")).toBeInTheDocument();
    expect(screen.getByText("Filter By Covid Rating")).toBeInTheDocument();
    expect(screen.getByText("Filter By Safety")).toBeInTheDocument();
    expect(screen.getByText("Filter By Population")).toBeInTheDocument();

  });
});

describe("Exact Filters Testing", () => {
  test("Ensure the Exact Filter for Cities Model renders", async () => {
    render (
      <MemoryRouter>
      <ExactFilterBar/>
      </MemoryRouter>
    );
    expect(screen.getByText("Filter By State")).toBeInTheDocument();
  });
});
