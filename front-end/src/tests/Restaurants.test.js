import { MemoryRouter } from "react-router-dom";
import { render, screen } from "@testing-library/react";
import Restaurants from "../components/restaurants/RestaurantModel";
import { FilterBar, SearchBar } from "../components/restaurants/RestaurantsToolBar";

describe("Restaurants page testing", () => {
    test("Restaurants page renders", async () => {
      render(
        <MemoryRouter>
          <Restaurants />
        </MemoryRouter>
      );
      const elem = screen.getByText("Restaurants");
      expect(elem).toBeInTheDocument();
    });
});

describe("Exact Filters Testing", () => {
  test("Ensure the Exact Filter for Restaurant Model renders", async () => {
    render (
      <MemoryRouter>
      <FilterBar/>
      </MemoryRouter>
    );
    expect(screen.getByText("Filter By State")).toBeInTheDocument();
    expect(screen.getByText("Filter By Rating")).toBeInTheDocument();
    expect(screen.getByText("Filter By Cuisine")).toBeInTheDocument();
    expect(screen.getByText("Filter By Pricing")).toBeInTheDocument();
  });
});

