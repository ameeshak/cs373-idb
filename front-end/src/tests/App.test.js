import { MemoryRouter } from "react-router-dom";
import { render, screen } from "@testing-library/react";
import App from "../App";

describe("App testing", () => {
    test("Render whole app", () => {
      render(<App/>);
      const titleElem = screen.getByText("Find Cities");
      expect(titleElem).toBeInTheDocument();
    });
  });
  