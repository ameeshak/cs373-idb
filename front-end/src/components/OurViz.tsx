import { Container, Stack, Typography } from "@mui/material"
import CityViz from "./visualizations/CityViz"
import EventTypeViz from './visualizations/EventTypeViz'
import RestaurantScatterPlot from "./visualizations/RestaurantScatterPlot";

export default function OurViz() {
    return (
        <Container className="page-container" sx={{ textAlign: "center" }}>
            <Stack justifyContent="center" direction="column" textAlign="center">
                <Typography className="modelTitle" variant="h2" gutterBottom>
                Visualizations
                </Typography>
                <Typography
                    gutterBottom
                    sx={{ marginTop: "48px", marginBottom: "8px" }}
                    variant="h4"
                >
                Average City Statistics by State
                </Typography>
                <CityViz />
                <Typography
                    gutterBottom
                    sx={{ marginTop: "48px", marginBottom: "8px" }}
                    variant="h4"
                >
                Types of Events
                </Typography>
                <Stack justifyContent={"center"} alignItems={"center"}>
                     <EventTypeViz></EventTypeViz>

                </Stack>
                <Typography
                    gutterBottom
                    sx={{ marginTop: "48px", marginBottom: "8px" }}
                    variant="h4"
                >
                Population of City and Restaurant Customer Review Count
                </Typography>
                <Stack justifyContent="center">
                     <RestaurantScatterPlot/>

                </Stack>

            </Stack>
        </Container>

    )
}
