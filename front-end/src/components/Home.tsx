export default function Home() {
    return (
        <div id="myCarousel" className="carousel slide" data-bs-ride="carousel">
            <div className="carousel-indicators">
                <button type="button" data-bs-target="#myCarousel" data-bs-slide-to="0" className="active" aria-current="true" aria-label="Slide 1"></button>
                <button type="button" data-bs-target="#myCarousel" data-bs-slide-to="1" aria-label="Slide 2"></button>
                <button type="button" data-bs-target="#myCarousel" data-bs-slide-to="2" aria-label="Slide 3"></button>
            </div>
            <div className="carousel-inner">
                <div className="carousel-item active">
                    <img className="d-block w-100" src="https://urbanmatter.com/austin/wp-content/uploads/2021/01/Ryan-Kyte-via-Visit-Austin.jpg" alt="First slide" style={{width: '100%', blockSize: '100%', objectFit:"contain"}} />

                    <div className="container">
                        <div className="carousel-caption text-start">
                            <h1>Find Cities</h1>
                            <p>Find cities that peak your interest. Sort by name, location, weather, and more!</p>
                            <p><a className="btn btn-lg btn-primary" href="/Cities">Explore</a></p>
                        </div>
                    </div>
                </div>
                <div className="carousel-item">
                    <img className="d-block w-100" src="https://mrhsridgereview.org/wp-content/uploads/2021/10/I2IUU5K2LOCDXNU5PTVNYG75ZI.jpg" alt="Second slide"  style={{width: '100%', blockSize: '100%', objectFit:"contain"}} />
                    <div className="container">
                        <div className="carousel-caption">
                            <h1>Find Events</h1>
                            <p>With Out-N-About you can find exciting events nearby</p>
                            <p><a className="btn btn-lg btn-primary" href="/Events">Learn more</a></p>
                        </div>
                    </div>
                </div>
                <div className="carousel-item">
                    <img className="d-block w-100" src="https://cdn.vox-cdn.com/thumbor/BEDuFv6Zxe5vjFSZSpAlJqcylO8=/0x0:3000x2000/1200x900/filters:focal(1260x760:1740x1240)/cdn.vox-cdn.com/uploads/chorus_image/image/70457701/Rusty_Pelican_dusk.0.jpg" alt="Third slide"  style={{width: '100%', blockSize: '100%', objectFit:"contain"}} />
                    <div className="container">
                        <div className="carousel-caption text-end">
                            <h1>Restaurants</h1>
                            <p>Find and experience some of the most delicious restaurants. Filter by food type, hours, accomodations, and more.</p>
                            <p><a className="btn btn-lg btn-primary" href="/Restaurants">Browse Restaurants</a></p>
                        </div>
                    </div>
                </div>
            </div>
            <button className="carousel-control-prev" type="button" data-bs-target="#myCarousel" data-bs-slide="prev">
                <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                <span className="visually-hidden">Previous</span>
            </button>
            <button className="carousel-control-next" type="button" data-bs-target="#myCarousel" data-bs-slide="next">
                <span className="carousel-control-next-icon" aria-hidden="true"></span>
                <span className="visually-hidden">Next</span>
            </button>
        </div>
    )
}