import React from 'react';
import { Pagination } from 'react-bootstrap'

const Paginate = (post: any, total: any, paginate: any) => {
  const pageNumbers = [];

  for (let i = 1; i <= Math.ceil(total / post); i++) {
    pageNumbers.push(i);
  }

  return (

    <div>
      <Pagination size='sm'>
        {pageNumbers.map(number => (
          <Pagination.Item key={number} href="#" onClick={() => paginate(number)}>
            {number}
          </Pagination.Item>
        ))}
      </Pagination>
    </div>
  );
}

export default Paginate
