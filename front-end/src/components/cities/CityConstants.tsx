const knownFor = [
    "Beach Town",
    "History",
    "Night Life",
    "Foodie",
    "Outdoorsy",
    "Shopping",
    "Performing Arts",
    "Museums",
    "Posh",
    "Hipster",
    "Hippie",
    "Charming",
    "College Town",
    "Family Friendly",
    "Ski Town",
    "Quiet",
    "Architecture",
    "LGBT Scene",
    "Wineries",
    "Diversity",
    "Music",
];



export default knownFor;