const CitySort = [
    {
        name: "population",
        display: "Population"
    },
    {
        name: "name",
        display: "Name"
    },
    {
        name : "costOfLiving",
        display: "Cost Of Living"
    },
    {
        name: "averageRating",
        display: "Average Rating"
    },
    {
        name: "covidRating",
        display: "Covid Rating",
    },
    {
        name: "safety",
        display: "Safety",
    },
]

const RangeCityFilters = [
    {
        name: "Cost Of Living",
        min: 0,
        max: 8,
        field: "budget"
    },
    {
        name: "Average Rating",
        min: 0,
        max: 5,
        field: "avg_rating"
    },
    {
        name: "Covid Rating",
        min: 0,
        max: 75,
        field: "covid"
    },
    {
        name: "Safety",
        min: 0,
        max: 5,
        field: "safety"
    },
    {
        name: "Population",
        min: 0,
        max: 8200000,
        field: "population"
    }
]



export { CitySort, RangeCityFilters};