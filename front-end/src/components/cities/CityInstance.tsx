import { Container, Spinner } from 'react-bootstrap';
import { useEffect, useState } from 'react';
import { Card, CardContent, Rating, Typography, CardActionArea, Button, Stack, Chip, Tooltip } from '@mui/material';
import axios from 'axios';
import { useParams } from "react-router-dom";


const CityInstance  = () => { 
    let us_state_to_abbrev = {
        "Alabama": "AL",
        "Alaska": "AK",
        "Arizona": "AZ",
        "Arkansas": "AR",
        "California": "CA",
        "Colorado": "CO",
        "Connecticut": "CT",
        "Delaware": "DE",
        "Florida": "FL",
        "Georgia": "GA",
        "Hawaii": "HI",
        "Idaho": "ID",
        "Illinois": "IL",
        "Indiana": "IN",
        "Iowa": "IA",
        "Kansas": "KS",
        "Kentucky": "KY",
        "Louisiana": "LA",
        "Maine": "ME",
        "Maryland": "MD",
        "Massachusetts": "MA",
        "Michigan": "MI",
        "Minnesota": "MN",
        "Mississippi": "MS",
        "Missouri": "MO",
        "Montana": "MT",
        "Nebraska": "NE",
        "Nevada": "NV",
        "New Hampshire": "NH",
        "New Jersey": "NJ",
        "New Mexico": "NM",
        "New York": "NY",
        "North Carolina": "NC",
        "North Dakota": "ND",
        "Ohio": "OH",
        "Oklahoma": "OK",
        "Oregon": "OR",
        "Pennsylvania": "PA",
        "Rhode Island": "RI",
        "South Carolina": "SC",
        "South Dakota": "SD",
        "Tennessee": "TN",
        "Texas": "TX",
        "Utah": "UT",
        "Vermont": "VT",
        "Virginia": "VA",
        "Washington": "WA",
        "West Virginia": "WV",
        "Wisconsin": "WI",
        "Wyoming": "WY",
        "District of Columbia": "DC",
        "American Samoa": "AS",
        "Guam": "GU",
        "Northern Mariana Islands": "MP",
        "Puerto Rico": "PR",
        "United States Minor Outlying Islands": "UM",
        "U.S. Virgin Islands": "VI",
      }

    let {id} : any = useParams()
    const [city, setCity] = useState(null);
    const [event, setEvent] = useState<any[]>([])
    const [rest, setRest] = useState<any[]>([])
    const eventsInCity = new Map();
    const restInCity = new Map();
    let event_id: any
    let event_name: any
    let rest_id: any
    let rest_name: any
    useEffect(() => {
        const fetchData = async () => {
            const response = await axios.get('https://outnabout.p3eas2rb02ene.us-east-2.cs.amazonlightsail.com/api/cities' + `/${id}`);
            setCity(JSON.parse(response.data));
        };
        fetchData(); 
        const getEvents = async() => {
            const response = await axios.get(`https://outnabout.p3eas2rb02ene.us-east-2.cs.amazonlightsail.com/api/events`);
            setEvent(response.data.data)
        };
        getEvents();
        const getRestaurants = async() => {
            const response = await axios.get(`https://outnabout.p3eas2rb02ene.us-east-2.cs.amazonlightsail.com/api/restaurants`);
            setRest(response.data.data)
        };
        getRestaurants();
   }, [city, event, rest])

    if (city === null || event === null) {
        (<Spinner animation="border" />)
    } else {        
        for (let e of event) {
            if (city["city_name"] === e["event_city"]) {
                eventsInCity.set("data", e)
                event_id = eventsInCity.get("data")["event_id"]
                event_name = eventsInCity.get("data")["event_name"]
            }
        }

        if(event_id == null){
            for(let e of event){
                if(city["city_state"] === e["event_state"]){
                    eventsInCity.set("data", e)
                    event_id = eventsInCity.get("data")["event_id"]
                    event_name = eventsInCity.get("data")["event_name"]
                }
            }
        }

    }

    if (city === null || rest === null) {
        (<Spinner animation="border" />)
    } else {
        console.log(us_state_to_abbrev)
        console.log(city["city_state"])
        let curr_state = us_state_to_abbrev[city["city_state"]]
        for (let r of rest) {
            if (city["city_name"] === r["rest_city"]) {
                restInCity.set("data", r)
                rest_id = restInCity.get("data")["rest_id"]
                rest_name = restInCity.get("data")["rest_name"]
            }
        }
        if (rest_id == null) {
            for (let r of rest) {
              if (curr_state === r["rest_state"]) {
                restInCity.set("data", r)
                rest_id = restInCity.get("data")["rest_id"]
                rest_name = restInCity.get("data")["rest_name"]
              }
            }
          }
    }

    return (
        <div className="section-container">
            {city === null ? (<Spinner animation="border" />) :
         (<div className="instance-content">
                        <Container >
                <Typography variant="h2" textAlign="center" lineHeight={2}>
                        {city["city_name"]}
                </Typography>
                <div className="row" style={{justifyContent: "space-evenly"}}>
                    <div className="col">
                        <img src={city["city_image"]} style={{height: 325, width: 500}}/>
                    </div>
                    <div className="col">
                        <Card>
                            <CardContent>
                                <Typography variant="h4" textAlign="center">
                                    {"Characteristics"}
                                </Typography>
                                <Typography variant="h6">
                                    {"State: " + city["city_state"]}
                                </Typography>
                                <Typography variant="h6">
                                    {"Population: " + city["city_population"] + " people"}
                                </Typography>
                                <Tooltip title="A range between 1-75 that indicates how prevalent covid is in a city. { <1: low, <=1: Medium, <=10: High, <=25: Critical, <=75: Extreme}">
                                    <Typography variant="h6">
                                        {"Covid Score: " + city["city_covid"] + " out of 75"}
                                    </Typography>
                                </Tooltip>
                                
                                <Tooltip title="A scale from 1-5 that reflects how safe it is to travel in this city. 
                                {1: Travel Not Advised, 2: Reconsider Travel, 3: Exercise Increased Precaution, 4 & 5: Exercise Normal Precaution}">
                                    <Typography variant="h6">
                                        {"Safety: "}
                                        <Rating name="read-only" value={city["city_safety"]} precision={0.1} readOnly/>
                                    </Typography>
                                </Tooltip>
                                
                                <Tooltip title="A scale from 1-8 that represents the daily cost of being at this location. 
                                    {1: Very Low, 2: Low, 3: Medium Low, 4: Medium, 5: Medium High, 6: High, 7: Very High, 8: Extreme}">
                                    <Typography variant="h6">
                                        {"Cost of Living: "}
                                        <Rating name="read-only" value={city["city_budget"]} max={8} precision={0.1} readOnly/>
                                    </Typography>
                                </Tooltip>
                                
                                <Typography variant="h6">
                                    {"Average Rating: "}
                                    <Rating name="read-only" value={city["city_avg_rating"]} precision={0.1} readOnly/>
                                </Typography>
                            </CardContent>
                        </Card>
                    </div>
                </div>
                <Typography variant='h3' textAlign="center" lineHeight={2}>
                    {"City Map"}
                </Typography>
                <div style={{textAlign:"center"}}>
                    <iframe src={`https://maps.google.com/maps?q=${city["city_name"]}&output=embed`} style={{height: 500, width: 500}}></iframe>
                </div>
                <Card>
                    <CardContent>
                        <Typography>
                        {"Upcoming Events Happening In This City"}
                        <CardActionArea
                            className="actionArea"
                            href={`/events/id=${event_id}`}
                        >
                            <Button>{event_name}</Button>
                        </CardActionArea>
                        </Typography>
                    </CardContent>
                </Card>
                <Card>
                    <CardContent>
                        <Typography>
                        {"Restaurants nearby"}
                        <CardActionArea
                            className="actionArea"
                            href={`/restaurants/id=${rest_id}`}
                        >
                            <Button>{rest_name}</Button>
                        </CardActionArea>
                        </Typography>
                    </CardContent>
                </Card>
            </Container>
        
         </div>
         )}
         </div>
    )
}

export default CityInstance
