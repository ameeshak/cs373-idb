interface City {
    city_id: string,
    city_name: string,
    city_state: string,
    city_budget: string,
    city_population: number,
    city_safety: string,
    city_avg_rating: number,
    city_covid: number,
    city_walk_score_url: string,
    city_known_for: [],
    city_image: string,
}

export default City