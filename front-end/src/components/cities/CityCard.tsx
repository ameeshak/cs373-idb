import { CardActionArea, CardContent, CardMedia, Chip, Rating, Stack, Tooltip, Typography } from '@mui/material';
import Card from '@mui/material/Card';
import { Button } from 'react-bootstrap';
import knownFor from './CityKnownFor';
import City from './CityInterface'
import Highlighter from "react-highlight-words";
interface CityCardProps {
    city: City,
    query: String
}
export default function CityCard(props: CityCardProps) {

    return (
            <Card>
                <CardContent>
                    <CardMedia image={props.city.city_image} id="city-image" style={{height:225, width: 400, display:"inline-block"}}/>
                    <Typography variant="h4">
                    <Highlighter
                        highlightClassName="highlighter"
                        searchWords={props.query?.split(" ") ?? []}
                        autoEscape={true}
                        textToHighlight={props.city.city_name}
                        />
                    </Typography>
                    <Typography variant="h6">
                    <Highlighter
                        highlightClassName="highlighter"
                        searchWords={props.query?.split(" ") ?? []}
                        autoEscape={true}
                        textToHighlight={props.city.city_state}
                        />
                    </Typography>
                    <Typography variant="subtitle1" style={{display:"flex"}} >
                        {"Average Rating: "}
                        <Rating name="read-only" value={props.city.city_avg_rating} precision={0.1} readOnly/>
                    </Typography>

                    <Tooltip title="A scale from 1-8 that represents the daily cost of being at this location. 
                                    {1: Very Low, 2: Low, 3: Medium Low, 4: Medium, 5: Medium High, 6: High, 7: Very High, 8: Extreme}">
                                    <Typography variant="subtitle1" style={{display:"flex"}}>
                                        {"Cost of Living: "}
                                        <Rating name="read-only" value={parseInt(props.city.city_budget)} max={8} precision={0.1} readOnly/>
                                    </Typography>
                                </Tooltip>
                    <Typography variant="subtitle1">
                        {"Population: " + props.city.city_population.toLocaleString()}
                    </Typography>
                    <Typography variant="subtitle1">
                        <Stack direction="row" spacing={0.5} style={{marginBlock:3}}>
                            {props.city.city_known_for.slice(0, 4).map((data) => (
                                <Chip label={knownFor[data["id"] - 1]} />
                            ))}
                        </Stack>
                    </Typography>
                    <Button>
                        <CardActionArea className="actionArea" href={`/cities/id=${props.city.city_id}`}>
                            Learn More
                        </CardActionArea>
                    </Button>
                </CardContent>
            </Card>

    )
}
