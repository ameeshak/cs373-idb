import CityCard from './CityCard';
import { Typography} from '@mui/material';
import { useEffect, useMemo, useState } from 'react';
import axios from 'axios';
import { Link, useSearchParams } from "react-router-dom";
import Paginate from '../../Paginate'
import { CitiesToolBar } from './CitiesToolBar';



const CitiesModel  = () => {
    const [cities, setCities] = useState([]);
    const [currentPage, setCurrentPage] = useState(1);
    const [postPerPage] = useState(9);
    const [searchParams, setSearchParams] = useSearchParams();
    const [paginate, setPaginate] = useState([]);

    useEffect(() => {
        const fetchData = async () => {
            const response = await axios.get('https://outnabout.p3eas2rb02ene.us-east-2.cs.amazonlightsail.com/api/cities?' + searchParams.toString() + "&page=" + currentPage);
            setCities(response.data.data);
            setPaginate(response.data.paginate);
        };
        fetchData();
    }, [searchParams, currentPage]);

    const Posts = () => {
        return (
            <h1>
                Cities
                <CitiesToolBar />
                <div className="container">
                    <div className="row row-cols-3" id="grid">
                        {paginate.map((c: any) => (
                        <Link to={`./id=${c.city_id}`}>
                            <div className="col">
                                <CityCard
                                city={c}
                                query={searchParams.get("search") ?? ""}
                                ></CityCard>
                            </ div>
                        </Link>
                        ))}
                    </div>
                </div>
            </h1>
        )
    }

    return (
        <div className='container mt-5'>
              <Posts />
              <Paginate id="page-bar"
                  currentPage={currentPage}
                  totalCount={cities.length}
                  pageSize={postPerPage}
                  onPageChange={(page: any) => setCurrentPage(page)}
              />

              <Typography variant="subtitle1" textAlign="center" lineHeight={3}>
                  {"Total Results: " + cities.length}
              </Typography>
              <Typography variant="subtitle1" textAlign="center" >
                  {"Page " + currentPage + " of " + Math.ceil(cities.length / postPerPage)}
              </Typography>
        </div>
    );
}

export default CitiesModel
