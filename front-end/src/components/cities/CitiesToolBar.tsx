import { Button, FormControl, IconButton, InputLabel, Menu, MenuItem, OutlinedInput, Select, SelectChangeEvent, Slider, Stack, TextField, Typography} from '@mui/material';
import SearchIcon from '@mui/icons-material/Search';
import { RangeCityFilters, CitySort } from './CityFilters';
import { Row } from 'react-bootstrap';
import { States } from './CityStates';
import React, { useCallback, useEffect, useMemo, useState } from "react";
import { useSearchParams } from "react-router-dom";
import { useLocation, useNavigate } from "react-router";
import qs from "query-string";


const SearchBar = ({}) => {
    const [searchParams, setSearchParams] = useSearchParams();
    const [searchQuery, setSearchQuery] = useState("");

    const submitSearch = (newSearchQuery : any) => {
        let newParams = searchParams;
        if (newSearchQuery.length === 0) {
            newParams.delete("search")
        } else {
            newParams.set("search", newSearchQuery);
        }
        setSearchParams(newParams);
    };
    
    const updateSearch = (searchValue : any) => {
        setSearchQuery(searchValue.target.value);
    }

    useEffect(() => {
        setSearchQuery(searchParams.get("search") ?? "");
    }, []);

    return (
        <form style = {{width: '59.5%'}}  >
            <TextField 
                id="search-bar"
                className="text"
                value={searchQuery}
                onChange={updateSearch}
                onKeyPress={(event) => {
                    if (event.key === "Enter") {
                        submitSearch(searchQuery);
                        window.location.href = window.location.href;
                    }
                }}        
                label="Search"
                variant="outlined"
                placeholder="Search..."
                InputProps={{
                    endAdornment: (
                        <IconButton type="submit" aria-label='search'>
                        <SearchIcon />
                        </IconButton>
                    )
                }}
                style = {{width: '100%'}}  
            />
        </form>
    );
}

const SortBar = () => {

    const [sortQuery, setSortQuery] = useSearchParams();

    const citySortQueries: { [id: string]: string; } = {
        "Population": "population",
        "Name": "name",
        "Cost Of Living": "budget",
        "Average Rating": "avg_rating",
        "Covid Rating": "covid",
        "Safety": "safety"
    };

    const handleSort = (event : any) => {

        let curSort: string = event.target.value;
        console.log("HANDLING SORT", curSort);

        sortQuery.delete("sort");
        sortQuery.delete("page");
        sortQuery.delete("desc");

        if (curSort in citySortQueries) {
            sortQuery.set("sort", citySortQueries[curSort]);
            sortQuery.set("page", "1");
            if (curSort == "Population" || curSort == "Average Rating" || curSort == "Covid Rating" || curSort == "Safety")
                sortQuery.set("desc", "true")
        }
        
        setSortQuery(sortQuery);
        sessionStorage.setItem("sort", curSort);
        window.location.reload()
    };

    return (
        <TextField
            id="outlined-sort-cities"
            select
            label={"Sort"}
            value={sessionStorage.getItem("sort") == "" ? "Sort" : sessionStorage.getItem("sort")}
            onChange={(event) => handleSort(event)} 
            style = {{width: '19%'}}
        >
            {CitySort.map((option, index) => (
            <MenuItem 
                key={option.display} 
                value={option.display}
            >
                {option.display}
            </MenuItem>
            ))}
        </TextField>
    );      
}

const ExactFilterBar = ({}) => {
    const navigate = useNavigate();
    const { pathname, search } = useLocation();
    const params = useMemo(() => qs.parse(search), [search]);
    const [filter, setFilter] = useState<string[]>([]);
    

    const changeFilters = useCallback(
        (elements : any) => {
            if (elements.length === 0) {
                delete params["state"]
                navigate({
                    pathname: pathname});
            } else {
                navigate({
                    pathname: pathname,
                    search: qs.stringify({
                        ...params,
                        ["state"]: elements.toString(),
                })});
            }
            sessionStorage.setItem("filters", JSON.stringify(elements));
            window.location.href = window.location.href;
        },
        [navigate, params]
    );

    const handleChange = (event: SelectChangeEvent<typeof filter>) => {
        const {
            target: { value },
        } = event;
        const elements = typeof value === 'string' ? value.split(',') : value;
        console.log(elements)
        setFilter(elements);
        changeFilters(elements);
    };

    const MenuProps = {
        PaperProps: {
          style: {
            maxHeight: 48 * 4.5 + 8,
            width: 250,
          },
        },
      };

    return (
                <FormControl sx={{ m: 1, width: '19%' }}>
                    <InputLabel>Filter By State</InputLabel>
                    <Select
                    multiple
                    value={JSON.parse(sessionStorage.filters || null) ?
                        JSON.parse(sessionStorage.filters) : filter}
                    onChange={handleChange}
                    input={<OutlinedInput label="State" />}
                    MenuProps={MenuProps}
                    >
                    {States.map((state) => (
                        <MenuItem
                            key={state.label}
                            value={state.label}
                        >
                        {state.label}
                        </MenuItem>
                    ))}    
                    </Select>
                </FormControl>        
    )
}

const RangeFilterBar = ({}) => {
    const [min, setMin] = useState(0);
    const [max, setMax] = useState(0);
    const [searchParams, setSearchParams] = useSearchParams();
    
    return (
        <div>     
            <Row>
                { RangeCityFilters.map((f : any) => (
                    <FormControl sx={{ m: 1, width: '18.75%' }}>
                        <InputLabel> {"Filter By " + f.name}</InputLabel>
                        <Select>
                            <div >
                                <TextField label={"min: " + f.min} size="small" style={{width: 100}} onChange={(e : any) => setMin(e.target.value)} />
                                -
                                <TextField label={"max: " + f.max} size="small" style={{width: 100}} onChange={(e : any) => setMax(e.target.value)}/>                            
                                <Button 
                                    onClick={() => {
                                        let newParams = searchParams;
                                        newParams.set(f.field, min.toString() + "-" + max.toString());
                                        if (min === f.min) {
                                            newParams.delete(f.field);
                                        }
                                        if (max === f.max) {
                                            newParams.delete(f.field);
                                        }
                                        setSearchParams(newParams);
                                        window.location.href = window.location.href;
                                    }}>
                                    Apply
                                </Button>
                            </div>
                        </Select>
                    </FormControl>
                ))}
            </Row>
        </div>
        
    )
}

const CitiesToolBar = ({}) => {
    return (
        <div>
            <Stack direction="column">
                <Stack direction="row" spacing={2}>
                    <SearchBar />
                    <SortBar />
                    <ExactFilterBar />
                </Stack>
            </Stack>
            <RangeFilterBar />
        </div>  
    );
}

export {CitiesToolBar, ExactFilterBar, RangeFilterBar, SearchBar, SortBar}
