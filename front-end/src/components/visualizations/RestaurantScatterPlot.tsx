

import { getFormControlUnstyledUtilityClass } from "@mui/base";
import axios from "axios";
import React, { useEffect, useState } from "react";
import {
    ScatterChart,
    Scatter,
    XAxis,
    YAxis,
    CartesianGrid,
    Label,
    Tooltip,
  } from "recharts";
import { Stack, Typography } from "@mui/material";

function RestaurantScatterPlot(){
    const [data, setData] = useState([{}]);
    useEffect(() => {
        const fetchData = async () => {
            const response = await axios.get('https://outnabout.p3eas2rb02ene.us-east-2.cs.amazonlightsail.com/api/restaurants');
            const response_city  = await axios.get('https://outnabout.p3eas2rb02ene.us-east-2.cs.amazonlightsail.com/api/cities');
            let rest_data = response.data.data;
            let city_data = response_city.data.data;
            let dict: any[] = [];
            let final_dict: any[] = [];

            city_data.forEach((city: any) => {
                let curr = city.city_name
                let val = city.city_population

                dict[curr] = val
            });
            rest_data.forEach((rest: any) => {
                let curr_city = rest.rest_city;
                if(curr_city in dict){
                    if(dict[curr_city] <= 1000000){
                        final_dict.push({x: rest.rest_review_count, y: dict[curr_city]})
                    }
                }
            });

            setData(final_dict)

        };
        fetchData();

    }, []);


    return (
        <Stack justifyContent="center" alignItems={'center'}>
         <ScatterChart
         width={500}
         height={400}
         margin={{
            top: 20,
            right: 30,
            left: 30,
            bottom: 30,
          }

          }>
            <CartesianGrid />
            <XAxis type="number" dataKey="y" domain={[0, 9467]}>
            <Label
                value="Population of Cities "
                offset={-16}
                position="insideBottom"
                textAnchor="middle"
                />
                </XAxis>
            <YAxis type="number" dataKey="x" domain={[0, 10000] }>
                <Label
                    angle = {-90}
                    value="Review Count"
                    position="insideLeft"
                    textAnchor="middle"
                />
            </YAxis>
            <Scatter data={data} fill="green" />
        </ScatterChart>

        </Stack>
    )
}

export default RestaurantScatterPlot