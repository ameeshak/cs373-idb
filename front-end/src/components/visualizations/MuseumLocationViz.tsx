import axios, { AxiosResponse } from "axios"
import { memo, useEffect, useState } from "react"
import { Stack, Typography } from "@mui/material"
import { ComposableMap, Geographies, Geography, Marker, ZoomableGroup} from "react-simple-maps"

type Props = {
    setTooltipContent: React.Dispatch<React.SetStateAction<any>>;
};

const MuseumLocationViz: React.FC<Props> = ({ setTooltipContent }) => {
    let [data, setData] = useState<any[]>([])

    useEffect(() => {
        if (data.length === 0) {
    
          const fetchData = async () => {
            let markers: any[] = []
            let response: AxiosResponse<any, any> = await axios.get(
              `https://api.artdb.me/museums?per_page=1000`
            );
            for (let museum of response.data["data"]) {
                markers.push({
                    markerOffset: 0,
                    name: museum["name"],
                    coordinates: [museum["longitude"], museum["latitude"]]        
                })
            }
            setData(markers);
          };
    
          fetchData();
        }
      }, [data]);
    return (
    <Stack justifyContent="center">
    <Typography sx={{ margin: "16px" }} variant="h4">
    Museum Locations
    </Typography>
    <div data-tip="">
        <ComposableMap>
            <ZoomableGroup>
                <Geographies geography="https://cdn.jsdelivr.net/npm/world-atlas@2/countries-110m.json">
                    {({ geographies }) =>
                    geographies.map((geo : any) => (
                        <Geography
                        key={geo.rsmKey}
                        geography={geo}
                        style={{
                            default: {
                            fill: "#D6D6DA",
                            outline: "none"
                            }, 
                            hover: {
                                fill: "#D6D6DA",
                                outline: "none"
                            }
                        }}
                        />
                    ))
                    }
                </Geographies>
                {data.map(({ name, coordinates, markerOffset }) => (
                    <Marker coordinates={coordinates}
                        onMouseEnter={() => {
                            setTooltipContent(`${name}`);
                        }}
                            onMouseLeave={() => {
                            setTooltipContent("");
                        }}
                    >
                    <circle r={3} fill="#F00" stroke="#fff" strokeWidth={1}/>
                    </Marker>
                ))}
            </ZoomableGroup>
        </ComposableMap>
    </div>
    </Stack>
)
}

export default memo(MuseumLocationViz)