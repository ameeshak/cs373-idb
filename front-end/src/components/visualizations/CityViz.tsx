import React, { useEffect } from "react";
import {
    Legend,
    ResponsiveContainer,
    RadarChart,
    Radar,
    PolarGrid,
    PolarAngleAxis,
    PolarRadiusAxis,
  } from "recharts";
import { Stack, TextField, MenuItem, Box } from "@mui/material";
import axios from "axios";
import {States} from "../cities/CityStates"

// referenced Univercity's StateRadarChartVisualization code
function CityViz(props : any) {
    let [currentState, setCurrentState] = React.useState<string>("Alabama");
    let [data, setData] = React.useState<any[]>([]);

    useEffect(() => {
        const fetchData = async () => {
            const response = await axios.get('https://outnabout.p3eas2rb02ene.us-east-2.cs.amazonlightsail.com/api/cities?state=' + `${currentState}`);
            var types = [ 
                {"name": "Average Rating", "value": 0},
                {"name": "Average Population", "value": 0},
                {"name": "Average Cost Of Living", "value": 0},
                {"name": "Average Safety", "value": 0}, 
                {"name": "Average Covid Rating", "value": 0}
            ];
            let count = 0;
            let cities = response.data.data
            cities.forEach((city: any) => {
                types[0].value += city.city_avg_rating
                types[1].value += city.city_population
                types[2].value += city.city_budget
                types[3].value += city.city_safety
                types[4].value += city.city_covid
                count++;
            });

            types[0].value = ((1.0 * types[0].value) / count) * 2
            types[1].value = ((1.0 * types[1].value) / count / 600000) * 10
            types[2].value = ((1.0 * types[2].value) / count / 8) * 10
            types[3].value = ((1.0 * types[3].value) / count) * 2
            types[4].value = ((1.0 * types[4].value) / count / 30) * 10
            
            setData(types)
        };
            fetchData();
        }, [currentState]);
      
        
    return (
        <Box marginTop="16px">
            <TextField
            id="filter-field"
            select
            label="State"
            value={currentState}
            onChange={(event) => setCurrentState(event.target.value)}
            InputProps={{
                sx: {
                borderRadius: "8px",
                backgroundColor: "grey",
                flexGrow: 1,
                minWidth: "150px",
                display: "flex",
                textAlign: "start",
                },
            }}
            >
            {States.map((option) => (
                <MenuItem key={option.label} value={option.label}>
                {option.label}
                </MenuItem>
            ))}
            </TextField>
            <Stack justifyContent="center">
            <ResponsiveContainer width="100%" height={300}>
                <RadarChart outerRadius={90} width={730} height={250} data={data}>
                <PolarGrid />
                <PolarAngleAxis dataKey="name" />
                <PolarRadiusAxis angle={18} domain={[0, 10]} />
                <Radar
                    name="Average"
                    dataKey="value"
                    stroke="green"
                    fill="green"
                    fillOpacity={0.5}
                />
                <Legend />
                </RadarChart>
            </ResponsiveContainer>
            </Stack>
        </Box>
        );
    }

export default CityViz;