import axios, { AxiosResponse } from "axios"
import { useEffect, useState } from "react"
import { Stack, Typography } from "@mui/material"
import { BarChart, Bar, XAxis, YAxis, Legend, Tooltip, ResponsiveContainer, Cell } from 'recharts'
  
export default function ArtistTimeViz() {
    let [data, setData] = useState<any[]>([])

    useEffect(() => {
        if (data.length === 0) {
            
            
    
            const fetchData = async () => {
                let buckets = [
                    {
                        "name": "< 1500",
                        "num": 0
                    },
                    {
                        "name": "1500-1599",
                        "num": 0
                    },
                    {
                        "name": "1600-1699",
                        "num": 0
                    },
                    {
                        "name": "1700-1799",
                        "num": 0
                    },
                    {
                        "name": "1800-1899",
                        "num": 0
                    },
                    {
                        "name": "> 1900",
                        "num": 0
                    }
                ]

                let response: AxiosResponse<any, any> = await axios.get(
                `https://api.artdb.me/artists?per_page=100`
                );

                console.log(response)
                for (let artist of response.data["data"]) {
                    let val = artist["birth_date"]
                    if (val) {
                        if (val < 1500) {
                            buckets[0]["num"] += 1
                        } else if (val < 1600) {
                            buckets[1]["num"] += 1
                        } else if (val < 1700) {
                            buckets[2]["num"] += 1
                        } else if (val < 1800) {
                            buckets[3]["num"] += 1
                        } else if (val < 1900) {
                            buckets[4]["num"] += 1
                        } else {
                            buckets[5]["num"] += 1
                        }
                    }
                }
                console.log(buckets)
                setData(buckets)
            };
            fetchData();
        }
      }, [data]);
    return (
    <Stack justifyContent="center">
    <Typography sx={{ margin: "16px" }} variant="h4">
    Artist Birth Dates
    </Typography>
    <ResponsiveContainer width="100%" height={400}>
        <BarChart width={730} height={250} data={data}>
        <XAxis dataKey="name" />
        <YAxis dataKey="num" />
        <Bar dataKey="num" fill="#8884d8" />
        </BarChart>
    </ResponsiveContainer>
    </Stack>


)
}