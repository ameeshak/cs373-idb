import axios, { AxiosResponse } from "axios"
import { useEffect, useState } from "react"
import { Stack, Typography } from "@mui/material"
import { PieChart, Pie, Legend, Tooltip, ResponsiveContainer, Cell } from 'recharts'
  
export default function ArtMediumViz() {
    let [data, setData] = useState<any[]>([])
    let [loading, setLoading] = useState<boolean>(false);

    const COLORS = ['#0088FE', '#00C49F', '#FFBB28', '#FF8042', '#9F2B68']

    useEffect(() => {
        if (data.length === 0) {
          setLoading(true);
    
          const fetchData = async () => {
            let temp: any[] = []
            let response: AxiosResponse<any, any> = await axios.get(
              `https://api.artdb.me/artworks?per_page=200`
            );
            let map: Map<string, number> = new Map<string, number>()
            for (let artwork of response.data["data"]) {
                map.set(artwork["type"], (map.get(artwork["type"]) ?? 0) + 1)
            }
            map.forEach((value: number, key: string) => {
                if ((map.get(key) ?? 0) > 8)
                  temp.push({ name: key, value: map.get(key) });
            })

            setData(temp)
            console.log(map)
            console.log(temp)
            setLoading(false)
          };
    
          fetchData();
        }
      }, [data]);
      const RADIAN = Math.PI / 180;
      const renderCustomizedLabel = (props:any) => {
        const radius = props.innerRadius + (props.outerRadius - props.innerRadius) * 0.5;
        const x = props.cx + radius * Math.cos(-props.midAngle * RADIAN);
        const y = props.cy + radius * Math.sin(-props.midAngle * RADIAN);
      
        return (
          <text x={x} y={y} fill="white" textAnchor={x > props.cx ? 'start' : 'end'} dominantBaseline="central">
            {`${(props.percent * 100).toFixed(0)}%`}
          </text>
        );
      };
    return (
    <Stack justifyContent="center">
    <Typography sx={{ margin: "16px" }} variant="h4">
      Art Mediums
    </Typography>
    <Typography sx={{ margin: "16px" }} variant="caption">
      Hover over chart to view medium
    </Typography>
    <ResponsiveContainer width="100%" height={400}>
        <PieChart width={400} height={400}>
          <Pie
            dataKey="value"
            isAnimationActive={true}
            data={data}
            cx="50%"
            cy="50%"
            labelLine={false}
            outerRadius={150}
            fill="#8884d8"
            label= {renderCustomizedLabel}
            >
            {data.map((entry, index) => (
                <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
              ))}
            </Pie>
          <Tooltip />
        </PieChart>
      </ResponsiveContainer>
    </Stack>


)
}