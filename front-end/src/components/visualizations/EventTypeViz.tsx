import axios from "axios";
import React, { useEffect, useState } from "react";
import { PieChart, Pie, Cell, Tooltip } from "recharts";


export default function EventTypeViz() {
    var colorArray = ["#8884d8", "#82ca9d", "#FFBB28", "#FF8042"];
    const [pieData, setPieData] = useState([{}]);
    let renderLabel = function(entry: any) {
        return entry.name;
    }

    useEffect(() => {
        const fetchData = async () => {
            const response = await axios.get('https://outnabout.p3eas2rb02ene.us-east-2.cs.amazonlightsail.com/api/events');
            var types = [ {"name": "Arts & Theatre", "value": 0},
            {"name": "Film", "value": 0},
            {"name": "Music", "value": 0},
            {"name": "Sports", "value": 0}];
            let count = 0;
            let eventData = response.data.data;
            eventData.forEach((event: any) => {
                let currType = event.event_type;
                if (currType == 'Arts & Theatre') {
                    types[0].value += 1
                } else if (currType == 'Film') {
                    types[1].value += 1
                } else if (currType == 'Music') {
                    types[2].value += 1
                } else if (currType == 'Sports') {
                    types[3].value += 1
                }
                count++;
            });
            types.forEach(type => {
                type.value = type.value / count;
            });
            setPieData(types);
        };
        fetchData();

    }, []);

    const CustomTooltip = ({ active, payload, label }: any) => {
        if (active) {
          return (
            <div className="custom-tooltip"
            style={{
                backgroundColor: "#ffff",
                padding: "5px",
                border: "1px solid #cccc"
             }}>
              <p className="label">{`${payload[0].name} : ${(payload[0].value * 100).toFixed(2) + "%"}`}</p>
            </div>
          );
        }

        return null;
    };
    return (
        <PieChart width={500} height={400}>
            <Pie data={pieData} dataKey="value" nameKey="name" cx="50%" cy="50%" outerRadius={150} label={renderLabel}>
            {pieData.map((entry: any, index: any) => (
                <Cell key={`cell-${index}`} fill={colorArray[index]}/>
            ))}</Pie>
        <Tooltip content={<CustomTooltip />}/>
        </PieChart>
    );
}