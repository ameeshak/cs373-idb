import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import {CardActionArea, Rating, Chip, Stack} from '@mui/material';
import { Button } from 'react-bootstrap';

import Restaurant from './RestaurantInterface';
import Highlighter from "react-highlight-words"
import { States } from './CityStates';

interface RestaurantCardProps {
    res: Restaurant,
    query: String
}

export default function RestaurantCard(props: RestaurantCardProps){
    const states = {
        'AK': 'Alaska',
        'AL': 'Alabama',
        'AR': 'Arkansas',
        'AS': 'American Samoa',
        'AZ': 'Arizona',
        'CA': 'California',
        'CO': 'Colorado',
        'CT': 'Connecticut',
        'DC': 'District of Columbia',
        'DE': 'Delaware',
        'FL': 'Florida',
        'GA': 'Georgia',
        'GU': 'Guam',
        'HI': 'Hawaii',
        'IA': 'Iowa',
        'ID': 'Idaho',
        'IL': 'Illinois',
        'IN': 'Indiana',
        'KS': 'Kansas',
        'KY': 'Kentucky',
        'LA': 'Louisiana',
        'MA': 'Massachusetts',
        'MD': 'Maryland',
        'ME': 'Maine',
        'MI': 'Michigan',
        'MN': 'Minnesota',
        'MO': 'Missouri',
        'MP': 'Northern Mariana Islands',
        'MS': 'Mississippi',
        'MT': 'Montana',
        'NA': 'National',
        'NC': 'North Carolina',
        'ND': 'North Dakota',
        'NE': 'Nebraska',
        'NH': 'New Hampshire',
        'NJ': 'New Jersey',
        'NM': 'New Mexico',
        'NV': 'Nevada',
        'NY': 'New York',
        'OH': 'Ohio',
        'OK': 'Oklahoma',
        'OR': 'Oregon',
        'PA': 'Pennsylvania',
        'PR': 'Puerto Rico',
        'RI': 'Rhode Island',
        'SC': 'South Carolina',
        'SD': 'South Dakota',
        'TN': 'Tennessee',
        'TX': 'Texas',
        'UT': 'Utah',
        'VA': 'Virginia',
        'VI': 'Virgin Islands',
        'VT': 'Vermont',
        'WA': 'Washington',
        'WI': 'Wisconsin',
        'WV': 'West Virginia',
        'WY': 'Wyoming'
        }

    let typerest = props.res.rest_type
    let result = props.res
    console.log(result)

    return(
        <Card>
        <CardContent>
            <CardMedia image={props.res.rest_image_url} id="rest-image" style={{height:225, width: 400, display:"inline-block"}}/>
            <Typography variant="h4">
            <Highlighter
                        highlightClassName="highlighter"
                        searchWords={props.query?.split(" ") ?? []}
                        autoEscape={true}
                        textToHighlight={props.res.rest_name}
                        />
            </Typography>
            <Typography variant="subtitle1">
            <Highlighter
                        highlightClassName="highlighter"
                        searchWords={props.query?.split(" ") ?? []}
                        autoEscape={true}
                        textToHighlight={props.res.rest_type}
                        />
            </Typography>
            <Typography variant="subtitle1">
            <Highlighter
                        highlightClassName="highlighter"
                        searchWords={props.query?.split(" ") ?? []}
                        autoEscape={true}
                        textToHighlight={props.res.rest_city + ", " + props.res.rest_state}
                        />
            </Typography>
            <Typography variant="subtitle1" style={{display:"flex"}}>
                                        {"Rating: "}
                                        <Rating name="read-only" value={parseInt(props.res.rest_rating)} max={5} precision={0.1} readOnly/>
            </Typography>
            <Typography variant="subtitle1">
                Review Count : {props.res.rest_review_count}
            </Typography>
            <Typography variant="subtitle1">
                {props.res.rest_price}
            </Typography>
            <Button>
                <CardActionArea className="actionArea" href={`/cities/id=${props.res.rest_id}`}>
                    Learn More
                </CardActionArea>
            </Button>


        </CardContent>
    </Card>
    )
}




