interface Restaurant {
    rest_id: string,
    rest_name: string,
    rest_rating: any,
    rest_url: string,
    rest_transactions: string,
    rest_price: string,
    rest_city: string,
    rest_state:string,
    rest_location: string,
    rest_review_count: number,
    rest_hours: any,
    rest_type: string,
    rest_image_url: any
}

export default Restaurant