import RestaurantCard from './Restaurant_Card';
import Paginate from '../../Paginate'
import axios from 'axios';
import { Link, useSearchParams} from "react-router-dom";
import { useEffect, useState, useMemo } from 'react';
import {Typography} from '@mui/material';
import { RestaurantsToolBar } from './RestaurantsToolBar';

const RestaurantModel = () => {
    const [rest, setRest] = useState([]);
    const [currentPage, setCurrentPage] = useState(1);
    const [postPerPage] = useState(9);
    const [searchParams, setSearchParams] = useSearchParams();
    const [paginate, setPaginate] = useState([]);

    useEffect(() => {
        const fetchData = async () => {
            const response = await axios.get('https://outnabout.p3eas2rb02ene.us-east-2.cs.amazonlightsail.com/api/restaurants?' + searchParams.toString() + "&page=" + currentPage);
            setRest(response.data.data);
            setPaginate(response.data.paginate);
        };
        fetchData();
    }, [searchParams, currentPage]);

    const Posts = () => {
        return (
            <h1>
                <div style={{textAlign: "center"}}>
                Restaurants
                </div>
                <RestaurantsToolBar/>

                <div className="container">
                    <div className="row row-cols-3" id="grid">
                        {paginate.map((c:any) =>
                            <Link to={`./id=${c.rest_id}`}>
                                <div className="col">
                                    <RestaurantCard
                                    key={c.id}
                                    res={c}
                                    query={searchParams.get("search") ?? ""}
                                ></RestaurantCard>
                                </ div>
                            </Link>
                        )}
                    </div>
                </div>
            </h1>
        )

    }

    return (
        <div className='container mt-5'>
            <Posts />
            <Paginate id="page-bar"
                currentPage={currentPage}
                totalCount={rest.length}
                pageSize={postPerPage}
                onPageChange={(page: any) => setCurrentPage(page)}

            />
            <Typography variant="subtitle1" textAlign="center" lineHeight={3}>
                {"Total Results: " + rest.length}
            </Typography>
            <Typography variant="subtitle1" textAlign="center" >
                {"Page " + currentPage + " of " + Math.ceil(rest.length / postPerPage)}
            </Typography>
        </div>
    );
}

export default RestaurantModel