import RestaurantCard from './Restaurant_Card';
import Paginate from '../../Paginate'
import axios from 'axios';
import { Link } from "react-router-dom";
import { useEffect, useState, useMemo, useCallback } from 'react';
import { States } from '../cities/CityStates';
import { Row } from 'react-bootstrap';
import {RestaurantType} from './RestaurantsType';
import { FormControl, IconButton, InputLabel, Button, Menu, MenuItem, OutlinedInput, Select, SelectChangeEvent, Slider, Stack, TextField, Typography} from '@mui/material';
import SearchIcon from '@mui/icons-material/Search';
import { useLocation, useNavigate } from "react-router";
import { useSearchParams } from "react-router-dom";
import * as React from 'react';
import qs from "query-string";

const SearchBar = ({}) => {
    const [searchParams, setSearchParams] = useSearchParams();
    const [searchQuery, setSearchQuery] = useState("");

    const submitSearch = (newSearchQuery : any) => {
        let newParams = searchParams;
        if (newSearchQuery.length === 0) {
            newParams.delete("search")
        } else {
            newParams.set("search", newSearchQuery);
        }
        newParams.set("page", "1");
        setSearchParams(newParams);
    };
    const updateSearch = (searchValue : any) => {
        setSearchQuery(searchValue.target.value);
    }

    useEffect(() => {
        setSearchQuery(searchParams.get("search") ?? "");
    }, []);

    return (
        <form style = {{width: '100%'}}  >
            <TextField
                id="search-bar"
                className="text"
                value={searchQuery}
                onChange={updateSearch}
                onKeyPress={(event) => {
                    if (event.key === "Enter") {
                        submitSearch(searchQuery);
                        window.location.reload();
                    }
                }}
                label="Search"
                variant="outlined"
                placeholder="Search..."
                InputProps={{
                    endAdornment: (
                        <IconButton type="submit" aria-label='search'>
                        <SearchIcon />
                        </IconButton>
                    )
                }}
                style = {{width: '100%'}}
            />
        </form>
    );
}

const SortBar = ({}) => {
    const [sortQuery, setSortQuery] = useSearchParams();
    const RestSorting = [
        {
            name: "name",
            display: "Name"
        },
        {
            name: "review_count",
            display: "Review Count"
        },
        {
            name : "city",
            display: "City"
        },
        {
            name: "state",
            display: "State"
        },
        {
            name: "rating",
            display: "Rating",
        },
    ]


    const restSortingQueries: { [id: string]: string; } = {
        "Name": "name",
        "Review Count": "review_count",
        "City": "city",
        "State": "state",
        "Rating": "rating",
    };


    const handleSort = (event: any) => {
        let curSort: string = event.target.value;
        console.log("HANDLING SORT", curSort);

        sortQuery.delete("sort");
        sortQuery.delete("page");
        sortQuery.delete("desc");

        if (curSort in restSortingQueries) {
            sortQuery.set("sort", restSortingQueries[curSort]);
            sortQuery.set("page", "1");
            if (curSort == "Review Count" || curSort == "Rating")
                sortQuery.set("desc", "true")
        }

        setSortQuery(sortQuery);
        sessionStorage.setItem("sort", curSort);
        window.location.reload()
    };

    return (
        <TextField
            id="outlined-sort-cities"
            select
            label="Sort"
            value={sessionStorage.getItem("sort") == "" ? "Sort" : sessionStorage.getItem("sort")}
            onChange={(event) => handleSort(event)}
            style = {{width: '20%'}}>

            {RestSorting.map((option) => (
            <MenuItem
                key={option.display}
                value={option.display}
            >
                {option.display}
            </MenuItem>
            ))}
        </TextField>
    );
}


const FilterBar = ({}) => {
    const navigate = useNavigate();
    const { pathname, search } = useLocation();
    const params = useMemo(() => qs.parse(search), [search]);
    const [stateFilter, setStateFilter] = useState<string[]>([]);
    const [typeFilter, setTypeFilter] = useState<string[]>([]);
    const [ratingFilter, setRatingFilter] = useState<string[]>([]);
    const [pricingFilter, setPricingFilter] = useState<string[]>([]);
    const [transactionsFilter, setTransactionsFilter] = useState<string[]>([]);




    const rating = [5, 4.5, 4, 3.5, 3, 2.5, 2, 1.5, 1]
    const pricing = ['$', '$$', '$$$']

    const transactions = [
        { label: "Pick Up", field: "pickup" },
        { label: "Delivery", field: "delivery" },
        { label: "reservation", field: "restaurant_reservation" }
    ];

    const states = ["AL", "AK", "AZ", "AR", "CA", "CO", "CT", "DC", "DE", "FL", "GA",
                    "HI", "ID", "IL", "IN", "IA", "KS", "KY", "LA", "ME", "MD",
                    "MA", "MI", "MN", "MS", "MO", "MT", "NE", "NV", "NH", "NJ",
                    "NM", "NY", "NC", "ND", "OH", "OK", "OR", "PA", "RI", "SC",
                    "SD", "TN", "TX", "UT", "VT", "VA", "WA", "WV", "WI", "WY"]

    const changeFilters = useCallback(
        (elements : any, argument: string) => {
            if (elements.length === 0) {
                delete params[argument]
                navigate({
                    pathname: pathname});
            } else {
                navigate({
                pathname: pathname,
                search: qs.stringify({
                    ...params,
                    [argument]: elements.toString(),
                })});
            }
            sessionStorage.setItem(argument + "Filter", JSON.stringify(elements));
            window.location.href = window.location.href;
        },
        [navigate, params]
    );

    const handleChange = (argument: string) => (event: SelectChangeEvent<typeof stateFilter>) => {
        const {
            target: { value },
        } = event;
        const elements = typeof value === 'string' ? value.split(',') : value;
        console.log(elements)

        switch(argument) {
            case "state":
                setStateFilter(elements);
                break;
            case "type":
                setTypeFilter(elements);
                break;
            case "rating":
                setRatingFilter(elements);
                break;
            case "pricing":
                setPricingFilter(elements);
                break;
            case "transactions":
                setTransactionsFilter(elements);
                break;

        }
        changeFilters(elements, argument);
    };

    const ITEM_HEIGHT = 48;
    const ITEM_PADDING_TOP = 8;
    const MenuProps = {
        PaperProps: {
          style: {
            maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
            width: 250,
          },
        },
      };



    return (
        <div>
            <FormControl sx={{ m: 1, width: 300 }}>
                <InputLabel>Filter By State</InputLabel>
                <Select
                multiple
                value={JSON.parse(sessionStorage.stateFilter || null) ?
                    JSON.parse(sessionStorage.stateFilter) : stateFilter}
                onChange={handleChange("state")}
                input={<OutlinedInput label="State" />}
                MenuProps={MenuProps}
                >
                {states.map((state) => (
                    <MenuItem
                    key={state}
                    value={state}
                    >
                    {state}
                    </MenuItem>
                ))}
                </Select>
            </FormControl>

            <FormControl sx={{ m: 1, width: 300 }}>
                <InputLabel>Filter By Rating</InputLabel>
                <Select
                multiple
                value={JSON.parse(sessionStorage.ratingFilter || null) ?
                    JSON.parse(sessionStorage.ratingFilter) : ratingFilter}
                onChange={handleChange("rating")}
                input={<OutlinedInput label="Rating" />}
                MenuProps={MenuProps}
                >
                {rating.map((state) => (
                    <MenuItem
                    key={state}
                    value={state}
                    >
                    {state}
                    </MenuItem>
                ))}
                </Select>
            </FormControl>


            <FormControl sx={{ m: 1, width: 300 }}>
                <InputLabel>Filter By Cuisine</InputLabel>
                <Select
                multiple
                value={JSON.parse(sessionStorage.typeFilter|| null) ?
                    JSON.parse(sessionStorage.typeFilter) : typeFilter}
                onChange={handleChange("type")}
                input={<OutlinedInput label="State" />}
                MenuProps={MenuProps}
                >
                {RestaurantType.map((rest) => (
                    <MenuItem
                    key={rest}
                    value={rest}
                    >
                    {rest}
                    </MenuItem>
                ))}
                </Select>
            </FormControl>

            <FormControl sx={{ m: 1, width: 300 }}>
                <InputLabel>Filter By Pricing</InputLabel>
                <Select
                multiple
                value={JSON.parse(sessionStorage.pricingFilter || null) ?
                    JSON.parse(sessionStorage.pricingFilter) : pricingFilter}
                onChange={handleChange("pricing")}
                input={<OutlinedInput label="Pricing" />}
                MenuProps={MenuProps}
                >
                {pricing.map((price) => (
                    <MenuItem
                    key={price}
                    value={price}
                    >
                    {price}
                    </MenuItem>
                ))}
                </Select>
            </FormControl>
        </div>

    )
}



const RangeFilterBar = ({}) => {
    const [min, setMin] = useState(0);
    const [max, setMax] = useState(0);
    const [searchParams, setSearchParams] = useSearchParams();

    const RangeRestFilters = [
        {
            name: "Number Of Reviews",
            min: 0,
            max: 9467,
            field: "review_count"
        },
    ]
    return (
        <div>
            <Row>
                { RangeRestFilters.map((f : any) => (
                    <FormControl sx={{ m: 1, width: '18.75%' }}>
                        <InputLabel> {"Filter By " + f.name}</InputLabel>
                        <Select>
                            <div >
                                <TextField label={"min: " + f.min} size="small" style={{width: 100}} onChange={(e : any) => setMin(e.target.value)} />
                                -
                                <TextField label={"max: " + f.max} size="small" style={{width: 100}} onChange={(e : any) => setMax(e.target.value)}/>
                                <Button
                                    onClick={() => {
                                        let newParams = searchParams;
                                        newParams.set(f.field, min.toString() + "-" + max.toString());
                                        if (min === f.min) {
                                            newParams.delete(f.field);
                                        }
                                        if (max === f.max) {
                                            newParams.delete(f.field);
                                        }
                                        setSearchParams(newParams);
                                        window.location.href = window.location.href;
                                    }}>
                                    Apply
                                </Button>
                            </div>
                        </Select>
                    </FormControl>
                ))}
            </Row>
        </div>

    )
}


const RestaurantsToolBar = ({}) => {
    return (
        <div>
            <Stack direction="column">
                <Stack direction="row" spacing={2}>
                    <SearchBar />
                    <SortBar/>
                </Stack>
            </Stack>
            <FilterBar/>
            <RangeFilterBar/>
        </div>
    );
}

export {RestaurantsToolBar, RangeFilterBar, FilterBar, SearchBar}
