import { Container } from 'react-bootstrap';
import axios from 'axios';
import Moment from "moment";
import { useParams } from "react-router-dom";
import React, {useEffect, useState} from 'react';
import { Button, Card, CardContent, Rating, Typography, CardActionArea, Tooltip} from '@mui/material';
import { get } from 'https';
import Spinner from "react-bootstrap/Spinner";


export const RestaurantInstance = () => {

   const states = {
    'AK': 'Alaska',
    'AL': 'Alabama',
    'AR': 'Arkansas',
    'AS': 'American Samoa',
    'AZ': 'Arizona',
    'CA': 'California',
    'CO': 'Colorado',
    'CT': 'Connecticut',
    'DC': 'District of Columbia',
    'DE': 'Delaware',
    'FL': 'Florida',
    'GA': 'Georgia',
    'GU': 'Guam',
    'HI': 'Hawaii',
    'IA': 'Iowa',
    'ID': 'Idaho',
    'IL': 'Illinois',
    'IN': 'Indiana',
    'KS': 'Kansas',
    'KY': 'Kentucky',
    'LA': 'Louisiana',
    'MA': 'Massachusetts',
    'MD': 'Maryland',
    'ME': 'Maine',
    'MI': 'Michigan',
    'MN': 'Minnesota',
    'MO': 'Missouri',
    'MP': 'Northern Mariana Islands',
    'MS': 'Mississippi',
    'MT': 'Montana',
    'NA': 'National',
    'NC': 'North Carolina',
    'ND': 'North Dakota',
    'NE': 'Nebraska',
    'NH': 'New Hampshire',
    'NJ': 'New Jersey',
    'NM': 'New Mexico',
    'NV': 'Nevada',
    'NY': 'New York',
    'OH': 'Ohio',
    'OK': 'Oklahoma',
    'OR': 'Oregon',
    'PA': 'Pennsylvania',
    'PR': 'Puerto Rico',
    'RI': 'Rhode Island',
    'SC': 'South Carolina',
    'SD': 'South Dakota',
    'TN': 'Tennessee',
    'TX': 'Texas',
    'UT': 'Utah',
    'VA': 'Virginia',
    'VI': 'Virgin Islands',
    'VT': 'Vermont',
    'WA': 'Washington',
    'WI': 'Wisconsin',
    'WV': 'West Virginia',
    'WY': 'Wyoming'
    }


    const [rest, setRest] = useState(null)
    const [city, setCity] = useState<any[]>([])
    const [event, setEvent] = useState<any[]>([])
    const sameCity = new Map();
    const sameEvent  = new Map();
    let city_id: any
    let event_id: any
    let event_name: any
    const {restId} = useParams();
    console.log(restId)
    var hours_list:string[] =["Closed", "Closed", "Closed", "Closed", "Closed", "Closed","Closed"]
    var transactions_list = []
    var transactions_sentence = ""
    if(rest != null){
        // transactions_sentence = (rest["rest_transactions"][0]) + ", " + rest["rest_transactions"][1]


        for(let i = 0; i < 7; i++){
            if(rest['rest_hours'][i]){
                let x= rest['rest_hours'][i]['day']
                let num:number = x;
                hours_list[num] = Moment(rest['rest_hours'][i]['start'],  "hh:mm:ss").format("hh:mm A") + " - " +
                        Moment(rest['rest_hours'][i]['end'],  "hh:mm:ss").format("hh:mm A")
            }
        }

        for(let i = 0; i < 3; i++){
            if(rest["rest_transactions"][i] == null){
                break;
            }

            let temp = rest['rest_transactions'][i]
            let x:string = temp
            transactions_sentence += (x.charAt(0).toUpperCase() + x.slice(1))

            if(rest["rest_transactions"][i+1] != null){
                transactions_sentence += ", "
            }

        }
    }

    useEffect(() => {
        const fetchData = async () => {
            const response = await axios.get(`https://outnabout.p3eas2rb02ene.us-east-2.cs.amazonlightsail.com/api/restaurants/${restId}`);
            setRest(JSON.parse(response.data));
        };
        fetchData();

        // fetch cities information
        const getCity = async() => {
            const response = await axios.get(`https://outnabout.p3eas2rb02ene.us-east-2.cs.amazonlightsail.com/api/cities`);
            setCity(response.data.data)

        };
       getCity();

       // fetch event information
       const getEvent = async() => {
            const response = await axios.get(`https://outnabout.p3eas2rb02ene.us-east-2.cs.amazonlightsail.com/api/events`);
            setEvent(response.data.data)
        };
        getEvent();
        }, [rest, city, event]);

    if (city === null || rest === null) {
        (<Spinner animation="border" />)
      } else {
        let curr_city = rest["rest_city"]
        for (let c of city) {
          if (curr_city === c["city_name"]) {
            sameCity.set("data", c)
            city_id = sameCity.get("data")["city_id"]
          }
        }
    }
    if (rest === null || event === null) {
        (<Spinner animation="border" />)
      }
    else {
        let curr_city = rest["rest_city"]
        let curr_state = states[rest["rest_state"]]
        console.log(curr_state)
        for (let e of event) {
          if (curr_city === e["event_city"]) {
            sameEvent.set("data", e)
            event_id = sameEvent.get("data")["event_id"]
            event_name = sameEvent.get("data")["event_name"]
          }
        }

        if(event_id == null){
            for(let e of event){
                if(curr_state === e["event_state"]){
                    sameEvent.set("data", e)
                    event_id = sameEvent.get("data")["event_id"]
                    event_name = sameEvent.get("data")["event_name"]

                }
            }
        }
    }


    return (
        <div className="section-container">
            {rest === null ? (<h1>Null</h1>) :
            (
                <Container >
                    <Typography variant="h2" textAlign="center" lineHeight={2}>
                            {rest['rest_name']}
                    </Typography>
                    <div className="row" style={{justifyContent: "space-evenly"}}>
                    <div className="col">
                        <img src={rest['rest_image_url']} style={{height: 325, width: 350}}/>
                    </div>
                    <div className="col">
                        <Card>
                            <CardContent>
                                <Typography variant="h4" textAlign="center">
                                    {"More Details"}
                                </Typography>
                                <Typography variant="h6">
                                    {"City: " + rest['rest_city']}
                                </Typography>
                                <Typography variant="h6">
                                    {"State: " + states[rest['rest_state']]}
                                </Typography>
                                <Typography variant="h6">
                                    {"Location: " + rest['rest_address']}
                                </Typography>
                                <Typography variant="h6">
                                    {"Category: " + rest['rest_type']}
                                </Typography>
                                <Tooltip title="Approximate Price per person: $ = Less than $10, $$ = $11-$30, $$$= Greater than $31" placement='left'>
                                    <Typography variant="h6">
                                        {"Price: " + rest['rest_pricing']}
                                    </Typography>
                                </Tooltip>
                                <Typography variant="h6">
                                    {"Offers: " + transactions_sentence}
                                </Typography>
                                <Typography variant="h6">
                                    {"Hours Open:"}
                                    <br></br>
                                    {"Sunday: " +hours_list[0]}
                                    <br></br>
                                    {"Monday: " + hours_list[1]}
                                    <br></br>
                                    {"Tuesday: " +hours_list[2]}
                                    <br></br>
                                    {"Wednesday " +hours_list[3]}
                                    <br></br>
                                    {"Thursday " +hours_list[4]}
                                    <br></br>
                                    {"Friday " +hours_list[5]}
                                    <br></br>
                                    {"Saturday: " +hours_list[6]}

                                </Typography>
                                <CardActionArea className="actionArea" href={rest['rest_website']}>
                                    <Button>
                                        Go to Website
                                    </Button>
                                </CardActionArea>
                            </CardContent>
                            <Card>
                                <CardContent>
                                    <Typography>
                                    {"More about " + rest['rest_city']}
                                    <CardActionArea
                                        className="actionArea"
                                        href = {`/cities/id=${city_id}`}>
                                        <Button>Learn More</Button>
                                    </CardActionArea>
                                    </Typography>
                                </CardContent>
                            </Card>

                            <Card>
                                <CardContent>
                                    <Typography>
                                    {"Events in " + rest['rest_city']}
                                    <CardActionArea
                                        className="actionArea"
                                        href = {`/events/id=${event_id}`}
                                    >
                                        <Button>{event_name}</Button>
                                    </CardActionArea>
                                    </Typography>
                                </CardContent>
                            </Card>
                        </Card>
                    </div>
                </div>

                <Typography variant='h4' textAlign="center" lineHeight={5}>
                    {"Map of " + rest['rest_name']}
                </Typography>
                <div style={{textAlign:"center"}}>
                    <iframe src={`https://maps.google.com/maps?q=${rest['rest_address']}&output=embed`} style={{height: 500, width: 500}}></iframe>
                </div>

        </Container>
        )}
        </div>
    )
}

export default RestaurantInstance

