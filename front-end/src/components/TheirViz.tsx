import { Typography, Container, Stack } from "@mui/material"
import React, { useState } from "react";
import ReactTooltip from "react-tooltip";
import ArtMediumViz from "./visualizations/ArtMediumViz"
import MuseumLocationViz from "./visualizations/MuseumLocationViz"
import ArtistTimeViz from "./visualizations/ArtistTimeViz"

export default function TheirViz() {
    const [content, setContent] = useState("");
    return (
        <div>
        <Container className="page-container" sx={{ textAlign: "center" }}>
            <ArtMediumViz></ArtMediumViz>
            <MuseumLocationViz setTooltipContent={setContent}/>
            <ArtistTimeViz/>
            <ReactTooltip>{content}</ReactTooltip>
        </Container>
        </div>
    )
}