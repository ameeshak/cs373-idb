import { useNavigate, useSearchParams } from "react-router-dom"
import { API_URL } from "./globals"
import React, { useEffect } from "react"
import axios, { AxiosResponse } from "axios"
import City from "./cities/CityInterface"
import Event from "./events/EventInterface"
import { Container, Stack, Typography } from "@mui/material"
import CityCard from "./cities/CityCard"
import EventCard from "./events/EventModelCard"
import Restaurant from "./restaurants/RestaurantInterface"
import RestaurantCard from "./restaurants/Restaurant_Card"

export default function Search() {
    const [searchParams] = useSearchParams()
    const [cities, setCities] = React.useState<City[] | null>(null);
    const [events, setEvents] = React.useState<Event[] | null>(null);
    const [rests, setRests] = React.useState<Restaurant[] | null>(null);
    useEffect(() => {
        const fetchData = async () => {
        //   setColleges(null);
          let response: AxiosResponse<any, any> = await axios.get(
            `${API_URL}/api/search?` +
              searchParams.toString()
          );
          setCities(response.data["Cities"])
          setEvents(response.data["Events"])
          setRests(response.data["Restaurants"])
        };
        fetchData()
    }, [searchParams])
    // UI
    return (
        <Container
      className="page-container"
      sx={{
        display: "flex",
        flexDirection: "column",
      }}
    >
        <Typography
        gutterBottom
        className="modelTitle"
        variant="h2"
        sx={{ textAlign: "center" }}
      >
        Search
      </Typography>

      <Typography
        variant="h3"
        sx={{ padding: "24px 8px 8px 8px", textAlign: "center" }}
      >
        Cities
      </Typography>

        {cities !== null && (
        <Stack direction="row" flexWrap="wrap">
          {cities!.map((c) => (
            <CityCard
            city={c}
            query={searchParams.get("query") ?? ""}
            />
          ))}
        </Stack>
        )}

<Typography
        variant="h3"
        sx={{ padding: "24px 8px 8px 8px", textAlign: "center" }}
      >
        Events
      </Typography>

      {events !== null && (
        <Stack direction="row" flexWrap="wrap">
          {events!.map((e) => (
            <EventCard
            event={e}
            query={searchParams.get("query") ?? ""}
            />
          ))}
        </Stack>
        )}

<Typography
        variant="h3"
        sx={{ padding: "24px 8px 8px 8px", textAlign: "center" }}
      >
        Restaurants
      </Typography>

      {rests !== null && (
        <Stack direction="row" flexWrap="wrap">
          {rests!.map((r) => (
            <RestaurantCard
            res={r}
            query={searchParams.get("query") ?? ""}
            />
          ))}
        </Stack>
        )}

    </Container>

    )
}