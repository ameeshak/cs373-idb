import React from "react";
import axios from "axios";

class GroupMember {
    name: string;
    gitlab_names: string[];
    email: string;
    photo: string;
    bio: string;
    commits: number;
    issues: number;
    unittests: number;


    constructor(name: string, gitlab_names: string[], email: string, photo: string, bio: string, commits: number, issues: number, unittests: number) {
        this.name = name;
        this.gitlab_names = gitlab_names;
        this.email = email;
        this.photo = photo;
        this.bio = bio;
        this.commits = commits;
        this.issues = issues;
        this.unittests = unittests;
    }
}

export default class About extends React.Component {

    state = {
        memberList: [
            new GroupMember("Ameesha Kulkarni", ["Ameesha", "ameesha-k", "Ameesha Kulkarni"], "ameeshakulkarni@utexas.edu", "images/member-images/ameesha-image2.jpg",
                            "I’m a senior Computer Science major at UT Austin. I'm from Houston, Texas, and I enjoy going to concerts, \
                            painting, reading, and exploring new places in Austin!",
                            0, 0, 0),
            new GroupMember("Hannah Kim", ["hannah.kim99", "Hannah Kim"], "hannah.kim99@gmail.com", "images/member-images/hannah-image.jpg",
                            "I'm a senior studying computer science at UT Austin. I grew up in Dallas, Texas and I like hanging out with my \
                            dog, listening to music, and going to cute coffee shops!",
                            0, 0, 0),
            new GroupMember("Isaiah Suarez", ["Isaiah Suarez"], "isaiah.suarez@icloud.com", "images/member-images/isaiah-image.jpg",
                            "I’m a senior Computer Science major at UT Austin. I'm from Bulverde, Texas, and I enjoy going to concerts, \
                            trying wineries, and hiking.",
                            0, 0, 20),
            new GroupMember("Julianne Vu", ["Julianne Vu"], "julievu@utexas.edu", "images/member-images/julie-image.jpg",
                            "I'm a fourth year computer science major at UT Austin. I'm from Houston, Texas, and I enjoy listening to music, \
                            watching tv shows, making iced matcha lattes, and playing with my dog in my free time.",
                            0, 0, 0),
            new GroupMember("Varun Gorti", ["Varun Gorti", "VarunGorti"], "varungorti@gmail.com", "images/member-images/varun-image.jpg",
                            "I am a senior computer science major at UT. I grew up in Austin and enjoy spending time outside, cooking, and \
                            playing board games.",
                            0, 0, 0),
        ],
        total_commits: 0,
        total_issues: 0,
        total_tests: 0,
        issue_data: [],
        commit_data: [],
        branches: []
    }

    // https://reactjs.org/docs/react-component.html#componentdidmount
    componentDidMount() {
        axios.get('https://gitlab.com/api/v4/projects/39656079/repository/branches')
            .then(response => {
                const branches = response.data;
                this.setState({branches});
                var cur_commit_data = []

            });

        axios.get('https://gitlab.com/api/v4/projects/39656079/repository/contributors')
            .then(response => {
                const commit_data = response.data
                this.setState({commit_data})
            });

        axios.get("https://gitlab.com/api/v4/projects/39656079/issues?state=closed&per_page=100")
            .then(response => {
                const issue_data = response.data;
                this.setState({issue_data})
            });
    }

    // https://reactjs.org/docs/react-component.html#componentdidupdate
    componentDidUpdate(_: any, prevState: any) {
        console.log("something updated")
        // Loop through all the issue data and update individual commits
        var total_commits = 0;
        var total_issues = 0;
        var total_tests = 0;

        if(prevState.commit_data.length !== this.state.commit_data.length) {
            this.state.commit_data.forEach((commit: any) => {
                this.state.memberList.forEach((member: GroupMember) => {
                    if (member.gitlab_names.includes(commit.name)) {
                        member.commits += commit.commits;
                        total_commits += commit.commits;
                    }
                })
            })
            this.setState({total_commits});

            this.state.memberList.forEach((member: GroupMember) => {
                total_tests += member.unittests;
            })
            this.setState({total_tests});
        }

        if(prevState.issue_data.length !== this.state.issue_data.length) {
            this.state.issue_data.forEach((issue: any) => {
                this.state.memberList.forEach((member: GroupMember) => {
                    if (member.gitlab_names.includes(issue.closed_by.name)) {
                        member.issues += 1;
                        total_issues += 1;
                    }
                })
            })
            this.setState({total_issues});
        }
    }

    render() {
        return (
            <>
                <h1 className="text-center">About</h1>

                <div className="container text-center">
                    <div className="row gx-3">
                        <div className="col">
                            <h2>Description</h2>
                                <p>
                                    Our website helps plan trips to new cities by connecting cities with both restaurants and events
                                    happening in the area. We can help answer questions like "what are the best restaurants in the city?"
                                    and "how should I plan to get around while I am there?". We can also help people choose new locations
                                    to live by providing information on social events around the area and scoring cities based on factors
                                    like walkability and other metrics.
                                </p>
                        </div>
                        <div className="col">
                            <h2>Results</h2>
                                <p>
                                    Our website can be used to find what cities are the most suitable for your upcoming trips! It can be used
                                    for things like locating your favorite chains in new cities or even just planning transit around events.
                                </p>
                        </div>
                    </div>
                </div>


                <div className="card-group">
                    {this.state.memberList.map((member: GroupMember) => {
                        return (
                            <div className="card">
                                <img src={member.photo} className="card-img-top" alt="..." style={{width:'100%', height:'20vw', objectFit:'contain'}}/>
                                <h5 className="card-title">{member.name}</h5>
                                <div className="card-body">{member.bio}</div>
                                <div className="card-text"><small className="text-muted">Commits: {member.commits}</small></div>
                                <div className="card-text"><small className="text-muted">Issues closed: {member.issues}</small></div>
                                <div className="card-text"><small className="text-muted">Unit tests contributed: {member.unittests}</small></div>

                            </div>);
                    })}
                </div>

                <h5>{this.state.total_commits} commits have been made to this project.</h5>
                <h5>{this.state.total_issues} issues have been closed.</h5>
                <h5>{this.state.total_tests} tests have been written.</h5>

                <h2 className="text-center">Tools</h2>
                <div className="container text-center">
                    <div className="row gx-3">
                        <div className="col">
                            <div className="card mb-3 h-100">
                                <div className="row g-0">
                                    <div className="col-md-4">
                                        <a href="https://aws.amazon.com/"><img src="images/tools-images/aws-image.png" className="img-fluid" id="amazon-pic" alt="..." style={{width: '100%', paddingTop:15}}/></a>
                                    </div>
                                    <div className="col-md-8">
                                    <div className="card-body">
                                        <h5 className="card-title">AWS</h5>
                                        <p className="card-text">AWS was used to host and deploy this website!</p>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="col">
                            <div className="card mb-3 h-100">
                                <div className="row g-0">
                                    <div className="col-md-4">
                                    <a href="https://getbootstrap.com/"><img src="images/tools-images/bootstrap-image.jpg" className="img-fluid rounded-start" alt="..." style={{width: '100%', paddingTop:12}}/></a>
                                    </div>
                                    <div className="col-md-8">
                                    <div className="card-body">
                                        <h5 className="card-title">Bootstrap</h5>
                                        <p className="card-text">Bootstrap is a frontend Javascript toolkit that let us avoid writing a lot of CSS from scratch.</p>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="col">
                            <div className="card mb-3 h-100">
                                <div className="row g-0">
                                    <div className="col-md-4">
                                    <a href="https://discord.com/"><img src="images/tools-images/discord-image.png" className="img-fluid rounded-start" alt="..." style={{width: '100%', paddingTop:0}}/></a>
                                    </div>
                                    <div className="col-md-8">
                                    <div className="card-body">
                                        <h5 className="card-title">Discord</h5>
                                        <p className="card-text">We used Discord as our main method of group collaboration and communication.</p>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="row gx-3">
                        <div className="col">
                            <div className="card mb-3 h-100">
                                <div className="row g-0">
                                    <div className="col-md-4">
                                    <a href="https://gitlab.com/ameeshak/cs373-idb"><img src="images/tools-images/gitlab-image.jpg" className="img-fluid rounded-start" alt="..." style={{width: '100%', paddingTop:0}}/></a>
                                    </div>
                                    <div className="col-md-8">
                                    <div className="card-body">
                                        <h5 className="card-title">Gitlab</h5>
                                        <p className="card-text">GitLab was used as our version control system, allowing us to split different tasks and store our codebase in one secure place.</p>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="col">
                            <div className="card mb-3 h-100">
                                <div className="row g-0">
                                    <div className="col-md-4">
                                    <a href="https://www.namecheap.com/"><img src="images/tools-images/namecheap-image.png" className="img-fluid rounded-start" alt="..." style={{width: '100%', paddingTop:30}}/></a>
                                    </div>
                                    <div className="col-md-8">
                                    <div className="card-body">
                                        <h5 className="card-title">Namecheap</h5>
                                        <p className="card-text">Namecheap is the website we used to secure our domain name.</p>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="col">
                            <div className="card mb-3 h-100">
                                <div className="row g-0">
                                    <div className="col-md-4">
                                    <a href="https://documenter.getpostman.com/view/23609195/2s83tGoBNF"><img src="images/tools-images/postman-image.jpg" className="img-fluid rounded-start" alt="..." style={{width: '100%', paddingTop:30}}/></a>
                                    </div>
                                    <div className="col-md-8">
                                    <div className="card-body">
                                        <h5 className="card-title">Postman</h5>
                                        <p className="card-text">Postman is an API platform we used to design our own public API.</p>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div className="row gx-3">
                        <div className="col">
                            <div className="card mb-3 h-100">
                                <div className="row g-0">
                                    <div className="col-md-4">
                                    <a href="https://prettier.io/"><img src="images/tools-images/prettier-image.png" className="img-fluid rounded-start" alt="..." style={{width: '100%', paddingTop:0}}/></a>
                                    </div>
                                    <div className="col-md-8">
                                    <div className="card-body">
                                        <h5 className="card-title">Prettier</h5>
                                        <p className="card-text">Prettier is an automated code formatter we used during development.</p>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="col">
                            <div className="card mb-3 h-100">
                                <div className="row g-0">
                                    <div className="col-md-4">
                                    <a href="https://reactjs.org/"><img src="images/tools-images/react-image.png" className="img-fluid rounded-start" alt="..." style={{width: '100%', paddingTop:0}}/></a>
                                    </div>
                                    <div className="col-md-8">
                                    <div className="card-body">
                                        <h5 className="card-title">React</h5>
                                        <p className="card-text">React is a JavaScript library for quickly building complex UIs!</p>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="col">
                            <div className="card mb-3 h-100">
                                <div className="row g-0">
                                    <div className="col-md-4">
                                    <a href="https://www.postgresql.org/"><img src="images/tools-images/postgreSQL-image.png" className="img-fluid rounded-start" alt="..." style={{width: '100%', paddingTop:0}}/></a>
                                    </div>
                                    <div className="col-md-8">
                                    <div className="card-body">
                                        <h5 className="card-title">PostgreSQL</h5>
                                        <p className="card-text">PostgreSQL is a popular open-source object-relational mapper (ORM) database system that extends SQL.</p>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="row gx-3">
                        <div className="col">
                            <div className="card mb-3 h-100">
                                <div className="row g-0">
                                    <div className="col-md-4">
                                    <a href="https://aws.amazon.com/lightsail/"><img src="images/tools-images/amazon-lightsail-image.png" className="img-fluid rounded-start" alt="..." style={{width: '100%', paddingTop:0}}/></a>
                                    </div>
                                    <div className="col-md-8">
                                    <div className="card-body">
                                        <h5 className="card-title">Amazon Lightsail</h5>
                                        <p className="card-text">Amazon Lightsail allows you to push containerized applications such as docker images and host them online.</p>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="col">
                            <div className="card mb-3 h-100">
                                <div className="row g-0">
                                    <div className="col-md-4">
                                    <a href="https://flask.palletsprojects.com/en/2.2.x/"><img src="images/tools-images/flask-image.png" className="img-fluid rounded-start" alt="..." style={{width: '100%', paddingTop:0}}/></a>
                                    </div>
                                    <div className="col-md-8">
                                    <div className="card-body">
                                        <h5 className="card-title">Flask</h5>
                                        <p className="card-text">Flask is a Python web framework that we used within our application for tasks like url routing.</p>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="col">
                            <div className="card mb-3 h-100">
                                <div className="row g-0">
                                    <div className="col-md-4">
                                    <a href="https://www.sqlalchemy.org/"><img src="images/tools-images/SQLAlchemy-image.png" className="img-fluid rounded-start" alt="..." style={{width: '100%', paddingTop:0}}/></a>
                                    </div>
                                    <div className="col-md-8">
                                    <div className="card-body">
                                        <h5 className="card-title">SQLAlchemy</h5>
                                        <p className="card-text">SQLAlchemy is a Python SQL toolkit that aims to provide the structure of SQL databases while maintaining the advantages of Python object collections.</p>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <h2 className="text-center">APIs</h2>
                <div className="container text-center">
                    <div className="row gx-3">
                        <div className="col">
                            <div className="card mb-3 h-100">
                                <div className="row g-0">
                                    <div className="col-md-4">
                                        <a href="https://www.roadgoat.com/business/cities-api"><img src="images/api-images/roadgoat-image.jpg" className="img-fluid img-thumbnail" id="roadgoat-pic" alt="..." style={{width: '100%', paddingTop:0}}/></a>
                                    </div>
                                    <div className="col-md-8">
                                    <div className="card-body">
                                        <h5 className="card-title">Roadgoat</h5>
                                        <p className="card-text">Roadgoat is the API we used to get information about cities. We called the API, cleaned the data, saved the
                                                                relevant information into a JSON file, and uploaded the information to a SQL database.
                                        </p>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="col">
                            <div className="card mb-3 h-100">
                                <div className="row g-0">
                                    <div className="col-md-4">
                                    <a href="https://developer.ticketmaster.com/products-and-docs/apis/getting-started/"><img src="images/api-images/ticketmaster-image.png" className="img-fluid img-thumbnail" alt="..." style={{width: '100%', paddingTop:30}}/></a>
                                    </div>
                                    <div className="col-md-8">
                                    <div className="card-body">
                                        <h5 className="card-title">Ticketmaster</h5>
                                        <p className="card-text">Ticketmaster is the API we used to get information about events. We called the API, cleaned the data, saved the
                                                                relevant information into a JSON file, and uploaded the information to a SQL database.</p>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="col">
                            <div className="card mb-3 h-100">
                                <div className="row g-0">
                                    <div className="col-md-4">
                                    <a href="https://www.yelp.com/developers/documentation/v3/get_started"><img src="images/api-images/yelp-image.png" className="img-fluid img-thumbnail" alt="..." style={{width: '100%', paddingTop:20}}/></a>
                                    </div>
                                    <div className="col-md-8">
                                    <div className="card-body">
                                        <h5 className="card-title">Yelp</h5>
                                        <p className="card-text">Yelp is the API we used to get information about restaurants. We called the API, cleaned the data, saved the
                                                                relevant information into a JSON file, and uploaded the information to a SQL database.</p>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </>
        )
    }
}

