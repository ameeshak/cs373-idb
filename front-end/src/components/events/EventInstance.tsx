import axios from "axios";
import React, { useEffect, useRef, useState } from "react";
import {useParams } from "react-router-dom";
import { Container} from "react-bootstrap";
import Spinner from "react-bootstrap/Spinner";
import {
  Card,
  CardContent,
  Typography,
  CardActionArea,
  Button,
} from "@mui/material";
import './style.css'

  
 export const EventInstance = () => {
    let us_state_to_abbrev: any = {
      "Alabama": "AL",
      "Alaska": "AK",
      "Arizona": "AZ",
      "Arkansas": "AR",
      "California": "CA",
      "Colorado": "CO",
      "Connecticut": "CT",
      "Delaware": "DE",
      "Florida": "FL",
      "Georgia": "GA",
      "Hawaii": "HI",
      "Idaho": "ID",
      "Illinois": "IL",
      "Indiana": "IN",
      "Iowa": "IA",
      "Kansas": "KS",
      "Kentucky": "KY",
      "Louisiana": "LA",
      "Maine": "ME",
      "Maryland": "MD",
      "Massachusetts": "MA",
      "Michigan": "MI",
      "Minnesota": "MN",
      "Mississippi": "MS",
      "Missouri": "MO",
      "Montana": "MT",
      "Nebraska": "NE",
      "Nevada": "NV",
      "New Hampshire": "NH",
      "New Jersey": "NJ",
      "New Mexico": "NM",
      "New York": "NY",
      "North Carolina": "NC",
      "North Dakota": "ND",
      "Ohio": "OH",
      "Oklahoma": "OK",
      "Oregon": "OR",
      "Pennsylvania": "PA",
      "Rhode Island": "RI",
      "South Carolina": "SC",
      "South Dakota": "SD",
      "Tennessee": "TN",
      "Texas": "TX",
      "Utah": "UT",
      "Vermont": "VT",
      "Virginia": "VA",
      "Washington": "WA",
      "West Virginia": "WV",
      "Wisconsin": "WI",
      "Wyoming": "WY",
      "District of Columbia": "DC",
      "American Samoa": "AS",
      "Guam": "GU",
      "Northern Mariana Islands": "MP",
      "Puerto Rico": "PR",
      "United States Minor Outlying Islands": "UM",
      "U.S. Virgin Islands": "VI",
    } 
    const [event, setEvent] = useState(null)
    const [city, setCity] = useState<any[]>([])
    const sameCity = new Map();
    const [rest, setRest] = useState<any[]>([])
    const sameRest= new Map();
    const eventId = useParams().id;
    let city_id: any
    let rest_id: any
    let rest_name: any
    useEffect(() => {
        const fetchData = async () => {
            const response = await axios.get(`https://outnabout.p3eas2rb02ene.us-east-2.cs.amazonlightsail.com/api/events/${eventId}`);
            setEvent(JSON.parse(response.data))
        };
        fetchData(); 
        const getCity = async() => {
             const response = await axios.get(`https://outnabout.p3eas2rb02ene.us-east-2.cs.amazonlightsail.com/api/cities`);
             setCity(response.data.data)
         };
        getCity();
        const getRest = async() => {
          const response = await axios.get(`https://outnabout.p3eas2rb02ene.us-east-2.cs.amazonlightsail.com/api/restaurants`);
          setRest(response.data.data)
        };
        getRest();
    }, [event, city, rest])
    if (city === null || event === null) {
      (<Spinner animation="border" />)
    } else {
      let curr_city = event["event_city"]
      for (let c of city) {
        if (curr_city === c["city_name"]) {
          sameCity.set("data", c)
          city_id = sameCity.get("data")["city_id"]
        }
      }
    }
    if (rest === null || event === null) {
      (<Spinner animation="border" />)
    } else {
      let curr_city = event["event_city"]
      let curr_state = us_state_to_abbrev[event["event_state"]]
      console.log(curr_state)
      for (let r of rest) {
        if (curr_city === r["rest_city"]) {
          sameRest.set("data", r)
          rest_id = sameRest.get("data")["rest_id"]
          rest_name = sameRest.get("data")["rest_name"]
        }
      }
      if (rest_id == null) {
        for (let r of rest) {
          if (curr_state === r["rest_state"]) {
            sameRest.set("data", r)
            rest_id = sameRest.get("data")["rest_id"]
            rest_name = sameRest.get("data")["rest_name"]
          }
        }
      }
    }
    return (
        <div className="section-container">
            {event === null ? (<Spinner animation="border" />) :
         (<div className="instance-content">
         <Container style={{ maxWidth: "800px" }}>
            <Typography className="paperTitle" variant="h3" align="center">
              {event['event_name']}
            </Typography>
            <div className="row" style={{justifyContent: "space-evenly"}}>
              <div className="col">
                  <img src={event['event_image']} className="center" style={{height: 325, width: 500}}/>
              </div>
              <div className="col">
                <Card>
                  <CardContent>
                    <Typography gutterBottom variant="h5" component="div">
                      {"Event Date"}
                    </Typography>
                    <Typography variant="body1" color="text.primary">
                      {event['event_date'] + " at " + event['event_time']}
                    </Typography>
                  </CardContent>
                </Card>
                <Card>
                  <CardContent>
                    <Typography gutterBottom variant="h5" component="div">
                      {"Event Details"}
                    </Typography>
                    <Typography variant="h6">
                      {"Location: " + event['event_city']+ ", " + event['event_state']}
                    </Typography>
                    <Typography variant="h6">
                      {"Event type: " + event['event_type']}
                    </Typography>
                    <Typography variant="h6">
                      {"Event venute: " + event['event_venue']}
                    </Typography> 
                    <Typography variant="h6">
                      {"Event address: " + event['event_address']}
                    </Typography> 
                    <Typography variant="h6">
                      {"Ticket website: "}
                      <a target="_blank" href={event['event_url']}>
                        {event['event_url']}
                      </a>
                    </Typography>
                  </CardContent>
                </Card>
                <Card>
                  <CardContent>
                    <Typography>
                      {"More about " + event['event_city']}
                       <CardActionArea
                        className="actionArea"
                        href={`/cities/id=${city_id}`}
                      >
                        <Button>Learn More</Button>
                      </CardActionArea>
                    </Typography>
                  </CardContent>
                </Card>
                <Card>
                  <CardContent>
                    <Typography>
                      {"Restaurants nearby"}
                      <CardActionArea
                        className="actionArea"
                        href={`/restaurants/id=${rest_id}`}
                      >
                        <Button>{rest_name}</Button>
                      </CardActionArea>
                    </Typography>
                  </CardContent>
                </Card>
                <Typography variant="h4" align="center">
                  {"Event Venue"}
                </Typography>
                <div style={{ textAlign: "center" }}>
                    {" "}
                  <iframe
                    src={`https://maps.google.com/maps?q=${event['event_venue']}&output=embed`}
                    style={{ height: 500, width: 500 }}
                  ></iframe>
                </div>
              </div>
            </div>
      </Container>
         </div>
         )}
         </div>
    )
}

export default EventInstance