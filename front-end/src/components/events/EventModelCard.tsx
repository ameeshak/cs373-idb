import React from "react";
import Moment from "moment";
import { Button } from "react-bootstrap";
import Event from "./EventInterface"
import Highlighter from "react-highlight-words"
import {
  CardActionArea,
  CardContent,
  CardMedia,
  Typography,
  Stack,
  Chip,
} from "@mui/material";
import './style.css'

interface EventCardProps {
  event: Event,
  query: String
}

export default function EventCard(props: EventCardProps) {
  return (
      <div className="EventCard">
          <CardContent>
              <CardMedia image={props.event.event_image} id="event-image" style={{height:225, width: 400, display:"inline-block"}}/>
              <Typography gutterBottom variant="h6" component="div">
              <Highlighter
                        highlightClassName="highlighter"
                        searchWords={props.query?.split(" ") ?? []}
                        autoEscape={true}
                        textToHighlight={props.event.event_name}
                        />
              </Typography>
              <Stack direction="row" spacing={1}>
              <Typography gutterBottom variant="body2" color="text.secondary">
              <Highlighter
                        highlightClassName="highlighter"
                        searchWords={props.query?.split(" ") ?? []}
                        autoEscape={true}
                        textToHighlight={props.event.event_city +
                          ", " +
                          props.event.event_state}
                        />
              </Typography>
              </Stack>
              <Stack direction="row" spacing={1}>
                <Typography variant="body2" color="text.secondary">
                  {props.event.event_date}
                </Typography>
               
              </Stack>
              <Stack direction="row" spacing={1}>
              <Typography variant="body2" color="text.secondary">
                  {(props.event.event_time == "TBD") ? "TBD" : Moment(props.event.event_time, "hh:mm:ss").format("hh:mm A") + " " + props.event.event_timezone}
              </Typography>
              </Stack>
              <Stack direction="row" spacing={1}>
              <Typography variant="body2" color="text.secondary">
              <Highlighter
                        highlightClassName="highlighter"
                        searchWords={props.query?.split(" ") ?? []}
                        autoEscape={true}
                        textToHighlight={props.event.event_venue}
                        />
              </Typography>
            </Stack>
            <Typography variant="body2" color="text.secondary">
                {"Price: $" + Number(props.event.event_price).toFixed(2)}
              </Typography>
            <CardActionArea
                className="actionArea"
                href={`/events/id=${props.event.event_id}`}
              >
              <Button>Learn More</Button>
            </CardActionArea>
          </CardContent>
      </div>
  )
}
