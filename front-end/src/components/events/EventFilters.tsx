const EventSort = [
    {
        name: "name",
        display: "Name"
    },
    {
        name: "date",
        display: "Date"
    },
    {
        name: "city",
        display: "City"
    },
    {
        name: "price",
        display: "Price"
    },
    {
        name: "time",
        display: "Time"
    }
]

const EventTypes = [
    "Arts & Theatre",
    "Film",
    "Music",
    "Sports"
];

const RangeEventFilters = [
    {
        name: "Price",
        min: 0,
        max: 300,
        field: "price",
        delimiter: "-"
    },
    {
        name: "Time",
        min: "00:00:00",
        max: "23:59:59",
        field: "time",
        delimiter: "-"
    },
    {
        name: "Date",
        min: "2022-09-17",
        max: "2023-07-26",
        field: "date",
        delimiter: ":"
    },
    
]




export {EventSort, EventTypes, RangeEventFilters};