interface Event {
    id: string;
    name: string;
    city: string;
    venue: string;
    type: string;
    time: string;
    date:string;
    state: string;
    address: string;
    url: string;
    image: string;
  }
  
  export default Event;
  