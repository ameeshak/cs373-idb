import EventCard from "./EventModelCard";
import { FormControl, IconButton, InputLabel, Menu, MenuItem, OutlinedInput, Select, SelectChangeEvent, Slider, Stack, TextField, Typography} from '@mui/material';
import { useEffect, useState, useMemo } from 'react';
import axios from 'axios';
import * as React from 'react';
import Paginate from '../../Paginate'
import { Link, useSearchParams } from "react-router-dom";
import SearchIcon from '@mui/icons-material/Search';
import { EventSort, EventTypes, RangeEventFilters } from './EventFilters';
import { States } from '../cities/CityStates';
import './style.css'
import { Button, Row } from 'react-bootstrap';
import { EventsToolBar } from './EventsToolBar';

const EventModel  = () => {
  const [events, setEvents] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [postPerPage] = useState(9);
  const [searchParams, setSearchParams] = useSearchParams();
  const [paginate, setPaginate] = useState([]);
  
  useEffect(() => {
    const fetchData = async () => {
        const response = await axios.get('https://outnabout.p3eas2rb02ene.us-east-2.cs.amazonlightsail.com/api/events?' + searchParams.toString() + "&page=" + currentPage);
        setEvents(response.data.data);
        setPaginate(response.data.paginate)
    };
    fetchData();
}, [searchParams, currentPage]);

  const Posts = () => {
      return (
        <>
            <h1>
                Events
                <EventsToolBar/>
                <div className="container">
                    <div className="row row-cols-3" id="grid">
                    {paginate.map((e: any) => (
                        <Link to={`./id=${e.event_id}`}>
                            <div className="col">
                            <EventCard
                            event={e}
                            query={searchParams.get("search") ?? ""}
                            />
                            </ div>
                        </Link>
                        ))}
                    </div>
                </div>
            </h1>
        </>
      )
  }

  return (
      <div className='container mt-5'>
            <Posts />
            <Paginate id="page-bar"
                currentPage={currentPage}
                totalCount={events.length}
                pageSize={postPerPage}
                onPageChange={(page: any) => setCurrentPage(page)}
            />
            
            <Typography variant="subtitle1" textAlign="center" lineHeight={3}>
                {"Total Results: " + events.length}
            </Typography>
            <Typography variant="subtitle1" textAlign="center" >
                {"Page " + currentPage + " of " + Math.ceil(events.length / postPerPage)}
            </Typography>
      </div>
  );

}


export default EventModel
