interface Event {
    event_id: string;
    event_name: string;
    event_city: string;
    event_venue: string;
    event_type: string;
    event_time: string;
    event_date:string;
    event_state: string;
    event_address: string;
    event_url: string;
    event_image: string;
    event_price: string;
    event_timezone: string;
  }

  export default Event