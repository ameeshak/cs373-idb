import { Button, FormControl, IconButton, InputLabel, Menu, MenuItem, OutlinedInput, Select, SelectChangeEvent, Slider, Stack, TextField, Typography} from '@mui/material';
import SearchIcon from '@mui/icons-material/Search';
import { RangeEventFilters, EventSort, EventTypes } from './EventFilters';
import { Row } from 'react-bootstrap';
import React, { useCallback, useEffect, useMemo, useState } from "react";
import { useSearchParams } from "react-router-dom";
import { useLocation, useNavigate } from "react-router";
import qs from "query-string";
import { States } from '../cities/CityStates';


const EventSearchBar = ({}) => {
    const [searchParams, setSearchParams] = useSearchParams();
    const [searchQuery, setSearchQuery] = useState("");

    const submitSearch = (newSearchQuery : any) => {
        let newParams = searchParams;
        if (newSearchQuery.length === 0) {
            newParams.delete("search")
        } else {
            newParams.set("search", newSearchQuery);
        }
        setSearchParams(newParams);
    };
    
    const updateSearch = (searchValue : any) => {
        setSearchQuery(searchValue.target.value);
    }

    useEffect(() => {
        setSearchQuery(searchParams.get("search") ?? "");
    }, []);

    return (
        <form style = {{width: '100%'}}  >
            <TextField 
                id="search-bar"
                className="text"
                value={searchQuery}
                onChange={updateSearch}
                onKeyPress={(event) => {
                    if (event.key === "Enter") {
                        submitSearch(searchQuery);
                        window.location.href = window.location.href;
                    }
                }}        
                label="Search"
                variant="outlined"
                placeholder="Search..."
                InputProps={{
                    endAdornment: (
                        <IconButton type="submit" aria-label='search'>
                        <SearchIcon />
                        </IconButton>
                    )
                }}
                style = {{width: '100%'}}  
            />
        </form>
    );
}

const EventSortBar = () => {

    const [sortQuery, setSortQuery] = useSearchParams();

    const eventSortQueries: { [id: string]: string; } = {
        "Name": "name",
        "Date": "date",
        "City": "city",
        "Price": "price",
        "Time": "time"
    };

    const handleSort = (event : any) => {

        let curSort: string = event.target.value;
        console.log("HANDLING SORT", curSort);

        sortQuery.delete("sort");
        sortQuery.delete("page");

        if (curSort in eventSortQueries) {
            sortQuery.set("sort", eventSortQueries[curSort]);
            sortQuery.set("page", "1");
        }
        
        setSortQuery(sortQuery);
        sessionStorage.setItem("sort", curSort);
        window.location.reload()
    };

    return (
        <TextField
            id="outlined-sort-cities"
            select
            label={"Sort"}
            value={sessionStorage.getItem("sort") == "" ? "Sort" : sessionStorage.getItem("sort")}
            onChange={(event) => handleSort(event)} 
            style = {{width: '20%'}}
        >
            {EventSort.map((option, index) => (
            <MenuItem 
                key={option.display} 
                value={option.display}
            >
                {option.display}
            </MenuItem>
            ))}
        </TextField>
    );      
}

const EventExactFilterBar = ({}) => {
    const navigate = useNavigate();
    const { pathname, search } = useLocation();
    const params = useMemo(() => qs.parse(search), [search]);
    const [stateFilter, setStateFilter] = useState<string[]>([]);
    const [typeFilter, setTypeFilter] = useState<string[]>([]);
    

    const changeFilters = useCallback(
        (elements : any, argument: string) => {
            if (elements.length === 0) {
                delete params[argument]
                navigate({
                    pathname: pathname});
            } else {
                navigate({
                pathname: pathname,
                search: qs.stringify({
                    ...params,
                    [argument]: elements.toString(),
                })});
            }
            sessionStorage.setItem(argument + "Filter", JSON.stringify(elements));
            window.location.href = window.location.href;
        },
        [navigate, params]
    );

    const handleChange = (argument: string) => (event: SelectChangeEvent<typeof stateFilter>) => {
        const {
            target: { value },
        } = event;
        const elements = typeof value === 'string' ? value.split(',') : value;
        console.log(elements)

        switch(argument) {
            case "state":
                setStateFilter(elements);
                break;
            case "type":
                setTypeFilter(elements);
                break;
        }
        changeFilters(elements, argument);
    };

    const MenuProps = {
        PaperProps: {
          style: {
            maxHeight: 48 * 4.5 + 8,
            width: 250,
          },
        },
      };

    return (
        <div>
            <Row>
                <FormControl sx={{ m: 1, width: '48%' }}>
                    <InputLabel>Filter By State</InputLabel>
                    <Select
                    multiple
                    value={JSON.parse(sessionStorage.stateFilter || null) ?
                        JSON.parse(sessionStorage.stateFilter) : stateFilter}
                    onChange={handleChange("state")}
                    input={<OutlinedInput label="State" />}
                    MenuProps={MenuProps}
                    >
                    {States.map((state) => (
                        <MenuItem
                            key={state.label}
                            value={state.label}
                        >
                        {state.label}
                        </MenuItem>
                    ))}    
                    </Select>
                </FormControl>

                <FormControl sx={{ m: 1, width: '49%' }}>
                    <InputLabel>Filter By Event Type</InputLabel>
                    <Select
                    multiple
                    value={JSON.parse(sessionStorage.typeFilter || null) ?
                        JSON.parse(sessionStorage.typeFilter) : typeFilter}
                    onChange={handleChange("type")}
                    input={<OutlinedInput label="State" />}
                    MenuProps={MenuProps}
                    >
                    {EventTypes.map((event_type) => (
                        <MenuItem
                            key={event_type}
                            value={event_type}
                        >
                        {event_type}
                        </MenuItem>
                    ))}    
                    </Select>
                </FormControl>
            </Row>        
        </div>
        
    )
}

const EventRangeFilterBar = ({}) => {
    const [min, setMin] = useState(0);
    const [max, setMax] = useState(0);
    const [searchParams, setSearchParams] = useSearchParams();
    
    return (
        <div>     
            <Row>
                { RangeEventFilters.map((f : any) => (
                    <FormControl sx={{ m: 1, width: '32%' }}>
                        <InputLabel> {"Filter By " + f.name}</InputLabel>
                        <Select>
                            <div >
                                <TextField label={"min: " + f.min} size="small" style={{width: 200}} onChange={(e : any) => setMin(e.target.value)} />
                                -
                                <TextField label={"max: " + f.max} size="small" style={{width: 200}} onChange={(e : any) => setMax(e.target.value)}/>                            
                                <Button 
                                    onClick={() => {
                                        let newParams = searchParams;
                                        newParams.set(f.field, min.toString() + f.delimiter + max.toString());
                                        if (min === f.min) {
                                            newParams.delete(f.field);
                                        }
                                        if (max === f.max) {
                                            newParams.delete(f.field);
                                        }
                                        setSearchParams(newParams);
                                        window.location.href = window.location.href;
                                    }}>
                                    Apply
                                </Button>
                            </div>
                        </Select>
                    </FormControl>
                ))}
            </Row>
        </div>
        
    )
}

const EventsToolBar = ({}) => {
    return (
        <div>
            <Stack direction="column">
                <Stack direction="row" spacing={2}>
                    <EventSearchBar />
                    <EventSortBar />
                </Stack>
            </Stack>
            <EventExactFilterBar />
            <EventRangeFilterBar />
        </div>  
    );
}

export {EventsToolBar, EventExactFilterBar, EventSearchBar, EventRangeFilterBar}
