import { usePagination } from './usePagination';
import Pagination from 'react-bootstrap/Pagination';
import './components/events/style.css';

const Paginate = (props: any) => {
  const {
    onPageChange,
    totalCount,
    currentPage,
    pageSize,
    siblingCount = 1,
    skipParam = 5
  } = props;

  const paginationRange = usePagination(
    totalCount,
    pageSize
  );

  const onNext = () => {
    onPageChange(currentPage + 1);
  };

  const onPrevious = () => {
    onPageChange(currentPage - 1);
  };

  const onNextSkip = () => {
    onPageChange(currentPage + skipParam);
  };

  const onPrevSkip = () => {
    onPageChange(currentPage - skipParam);
  };

  let lastPage = paginationRange[paginationRange.length - 1];
  let items = [];

  if (currentPage > skipParam) {
    items.push(
        <Pagination.First onClick={() => onPrevSkip()}/>
    )
  } else {
    items.push(
        <Pagination.First disabled/>
    )
  }

  if (currentPage > 1) {
    items.push(
        <Pagination.Prev onClick={() => onPrevious()}/>
    )
  } else {
    items.push(
        <Pagination.Prev disabled/>
    )
  }

  items.push(
    <Pagination.Item key={1} active={1 === currentPage} onClick={() => onPageChange(1)}>
      {1}
    </Pagination.Item>,
  );

  if (currentPage > siblingCount + 2) {
    items.push(
        <Pagination.Ellipsis />
    )
  }

  for (let number = Math.max(currentPage - siblingCount, 2); number <= Math.min(currentPage + siblingCount, lastPage - 1); number++) {
    items.push(
      <Pagination.Item key={number} active={number === currentPage} onClick={() => onPageChange(number)}>
        {number}
      </Pagination.Item>,
    );
  }

  if (currentPage < lastPage - siblingCount - 1) {
    items.push(
        <Pagination.Ellipsis />
    )
  }

  items.push(
    <Pagination.Item key={lastPage} active={lastPage === currentPage} onClick={() => onPageChange(lastPage)}>
      {lastPage}
    </Pagination.Item>,
  );

  if (currentPage < lastPage) {
    items.push(
        <Pagination.Next onClick={() => onNext()}/>
    )
  } else {
    items.push(
        <Pagination.Next disabled/>
    )
  }

  if (currentPage <= lastPage - skipParam) {
    items.push(
        <Pagination.Last onClick={() => onNextSkip()}/>
    )
  } else {
    items.push(
        <Pagination.Last disabled/>
    )
  }

  return (
    <div className="PageBar">
        <Pagination size="lg">
            {items}
        </Pagination>
    </div>
  );
};

export default Paginate;