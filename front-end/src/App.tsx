import './App.css'
import { NavBar } from './NavBar'
import {
  BrowserRouter as Router,
  Routes as Switch,
  Route,
  Link,
} from "react-router-dom";

import Home from './components/Home'

import Cities from "./components/cities/CitiesModel"
import Events from "./components/events/EventModel";
import EventInstance from "./components/events/EventInstance";
import RestauarantModel from './components/restaurants/RestaurantModel'
import Restaurant from './components/restaurants/RestaurantInstance'
import About from './components/About'
import Search from './components/Search'
import CityInstance from './components/cities/CityInstance'
import OurViz from './components/OurViz';
import TheirViz from './components/TheirViz';

function App() {
    return (
        <Router>
            <NavBar />
            <Switch>
                <Route path="/" element={<Home />} />
                <Route path="/cities" element={<Cities />} />
                <Route path="/cities/:id" element={<CityInstance />} />
                <Route path="/events" element={<Events />} />
                <Route path="/events/:id" element={<EventInstance />} />
                <Route path="/restaurants/:restId" element={<Restaurant />} />
                <Route path="/restaurants" element={<RestauarantModel />} />
                <Route path="/about" element={<About />} />
                <Route path="/search" element={<Search/>} />
                <Route path="/ourviz" element={<OurViz/>} />
                <Route path="/theirviz" element={<TheirViz/>} />
            </Switch>
        </Router>
    )

}

export default App;
