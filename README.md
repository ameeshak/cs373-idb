Canvas/Discord Group Number:  11-1

Names of the team members:  
Ameesha Kulkarni (GitLab ID: ameeshak | EID: ask2766)  
Isaiah Suarez (GitLab ID: isaiah.suarez | EID: is7592)  
Hannah Kim (GitLab ID: hannah.kim99 | EID: sk45345)  
Julianne Vu (GitLab ID: julievu | EID: jdv2333)  
Varun Gorti (GitLab ID: VarunGorti | EID: vrg586)  

### Phase 1
**Phase Leader**: Isaiah Suarez

**Estimated Completition Time**:
| Name             | Hours |
| ---------------  | ----- |
| Ameesha Kulkarni | 10     |
| Isaiah Suarez    | 7      |
| Hannah Kim       | 8     |
| Julianne Vu      | 9   |
| Varun Gorti      | 6 |

**Actual Completition Time**:
| Name             | Hours |
| ---------------  | ----- |
| Ameesha Kulkarni | 13     |
| Isaiah Suarez    | 10  |
| Hannah Kim       | 12     |
| Julianne Vu      | 12.5   |
| Varun Gorti      | 8  |

**Git SHA**: 562853283ef1f5cb9eae46469c9ff0c203087844

**Comments**: Great teamwork. Greater website.


### Phase 2
**Phase Leader**: Hannah Kim

**Estimated Completition Time**:
| Name             | Hours |
| ---------------  | ----- |
| Ameesha Kulkarni | 21     |
| Isaiah Suarez    | 20      |
| Hannah Kim       | 20     |
| Julianne Vu      | 21  |
| Varun Gorti      | 20 |

**Actual Completition Time**:
| Name             | Hours |
| ---------------  | ----- |
| Ameesha Kulkarni | 25.5     |
| Isaiah Suarez    | 26  |
| Hannah Kim       | 25     |
| Julianne Vu      | 25   |
| Varun Gorti      | 25  |

### Phase 3
**Phase Leader**: Ameesha Kulkarni

**Estimated Completition Time**:
| Name             | Hours |
| ---------------  | ----- |
| Ameesha Kulkarni | ?     |
| Isaiah Suarez    | 20      |
| Hannah Kim       | ?     |
| Julianne Vu      | ?  |
| Varun Gorti      | ? |

**Actual Completition Time**:
| Name             | Hours |
| ---------------  | ----- |
| Ameesha Kulkarni | ?    |
| Isaiah Suarez    | 24  |
| Hannah Kim       | ?    |
| Julianne Vu      | ?   |
| Varun Gorti      | ?  |

**Git SHA**: TBD


Name of the project: Out n About

URL of website: https://www.out-n-about.me  
URL of the GitLab repo: https://gitlab.com/ameeshak/cs373-idb  
URL of GitLab Pipelines: https://gitlab.com/ameeshak/cs373-idb/-/pipelines  
The proposed project:  
Out N About is a website that will help anyone locate a city they would like to visit and any events and restaurants nearby. This will be a great resource for anyone looking to visit a new place and wanting to find out more information about the city before going.

URLs of at least three disparate data sources:

https://developer.ticketmaster.com/

https://www.roadgoat.com/business/cities-api

https://www.walkscore.com/professional/walk-score-apis.php

https://www.yelp.com/developers/documentation/v3

Filter By:

Events (50,000 instances) - Ticketmaster
1. City
2. Type
3. Family Friendly
4. Price
5. Date

Restaurants (200,000 instances) - Yelp API
1. Rating
2. Type of Cuisine
3. Price range
4. Review count
5. Delivery

Cities (1,000 instances) - RoadGoat
1. Cost of Living
2. Average Rating 
3. Covid Rating 
4. Population
5. Safety


Search:

Events
1. Name
2. Type
3. Accessibility
4. Street address
5. State

Restaurants
1. Name
2. Reservation
3. City
4. State
5. Hours (Open/Closed)

Cities
1. City Name
2. Known For 
3. State
4. Walkability Score
5. Public Transportation Score 


2 Types of Media:

Events
   1. Picture of event
   2. Link to event details
   3. Seating map

Restaurants
   1. Picture of restaurant
   2. Picture of menu
   3. Social media (Instagram, Twitter?)

Cities
   1. Attractions/Pictures of the City
   2. Public Transit Map

How will it connect to other models?

Events: Finding events in a certain city, finding hotel near event to stay at

Restaurants: Nearby Hotels, Transportation routes to get to restaurant (connection to city), If chain, list of cities the restaurant exists in

City: List of hotels in the city, List of restaurants in the city


Questions our project answers:
1. Where can I find the best events in the city?
2. What city should I plan a vacation to?
3. What restaurants should I eat at while on vacation?
4. How should I plan to get around the city?


