from unittest import main, TestCase
from app import app

class TestModels(TestCase):
    def test_cities(self):
        test_client = app.test_client()
        req = test_client.get("/api/cities")
        self.assertEqual(req.status_code, 200)

    def test_city_id(self):
        test_client = app.test_client()
        req = test_client.get("/api/cities/id=10744428")
        self.assertEqual(req.status_code, 200)

    def test_restaurants(self):
        test_client = app.test_client()
        req = test_client.get("/api/restaurants")
        self.assertEqual(req.status_code, 200)

    def test_restaurant_id(self):
        test_client = app.test_client()
        req = test_client.get("/api/restaurants/id=0cSIn-JMltCtiQ30WgtouA")
        self.assertEqual(req.status_code, 200)

    def test_events(self):
        test_client = app.test_client()
        req = test_client.get("/api/events")
        self.assertEqual(req.status_code, 200)

    def test_event_id(self):
        test_client = app.test_client()
        req = test_client.get("/api/events/id=1AvbZpAGkSUUQoX")
        self.assertEqual(req.status_code, 200)
    

if __name__ == "__main__":
    main()
