from sqlalchemy import create_engine
from sqlalchemy import Column, String, Integer, Float, ARRAY, JSON, BOOLEAN
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
import json
import os
from init import db

# Define restaurant table
class Restaurant(db.Model):
    __tablename__ = 'Restaurants'

    rest_id = Column(String, primary_key=True)
    rest_name = Column(String)
    rest_type = Column(String)
    rest_rating = Column(Float)
    rest_review_count = Column(Integer)
    rest_pricing = Column(String)
    rest_image_url = Column(String)
    rest_city = Column(String)
    rest_state = Column(String)
    rest_address = Column(ARRAY(String))
    rest_transactions = Column(ARRAY(String))
    rest_hours = Column(ARRAY(JSON))
    rest_website = Column(String)


    def __init__(self, rest_id="NaN", rest_name="NaN",rest_type="NaN",rest_rating=0.0,rest_review_count=0,rest_pricing="NaN",rest_image_url="NaN",
                rest_city="NaN",rest_state="NaN",rest_address=[],rest_transactions=[],rest_hours=[],rest_website="Nan" ):
        self.rest_id = rest_id
        self.rest_name = rest_name
        self.rest_type = rest_type
        self.rest_rating = rest_rating
        self.rest_review_count = rest_review_count
        self.rest_pricing = rest_pricing
        self.rest_image_url = rest_image_url
        self.rest_city = rest_city
        self.rest_state = rest_state
        self.rest_address = rest_address
        self.rest_transactions = rest_transactions
        self.rest_hours = rest_hours
        self.rest_website = rest_website

def create_table():
    db.create_all()

def populate_table():
    file_path = os.path.join(os.getcwd(), 'data/data.json')
    restaurant_file = open(file_path, 'r')
    restaurants = json.load(restaurant_file)
    rest_list = []

    for rest in restaurants:
        try:
            temp_hours = dict(rest['hours'][0])
            curr_rest = Restaurant(
                rest_id = rest["id"],
                rest_name = rest['name'],
                rest_type = rest['categories'][0]['title'],
                rest_rating = rest['rating'],
                rest_review_count = rest['review_count'],
                rest_pricing = rest['price'],
                rest_image_url = rest['image_url'],
                rest_city = rest['location']['city'],
                rest_state = rest['location']['state'],
                rest_address = rest['location']['display_address'],
                rest_transactions = rest['transactions'],
                rest_hours = temp_hours['open'],
                rest_website = rest['url']
                )
            rest_list.append(curr_rest)

        except:
            pass

    restaurant_file.close()
    return rest_list


def upload(list):
    Session = sessionmaker(db)
    session = Session()
    session.add_all(list)
    session.commit()
