from cities import City
from events import Events
from restaurants import Restaurant
from marshmallow_sqlalchemy import SQLAlchemySchema, auto_field

class CitySchema(SQLAlchemySchema):
    class Meta:
        model = City
        ordered = True
    city_id = auto_field()
    city_name = auto_field()
    city_population = auto_field()
    city_walk_score_url = auto_field()
    city_budget = auto_field()
    city_safety = auto_field()
    city_covid = auto_field()
    city_avg_rating = auto_field()
    city_known_for = auto_field()
    city_image = auto_field()
    city_state = auto_field()

class EventSchema(SQLAlchemySchema):
    class Meta:
        model = Events
        ordered = True
    event_id = auto_field()
    event_name = auto_field()
    event_city = auto_field()
    event_venue = auto_field()
    event_type = auto_field()
    event_time = auto_field()
    event_date= auto_field()
    event_state = auto_field()
    event_address = auto_field()
    event_url = auto_field()
    event_image = auto_field()
    event_accessibility = auto_field()
    event_price = auto_field()
    event_timezone = auto_field()

class RestaurantSchema(SQLAlchemySchema):
    class Meta:
        model = Restaurant
        ordered = True
    rest_id = auto_field() 
    rest_name = auto_field() 
    rest_type = auto_field() 
    rest_rating = auto_field() 
    rest_review_count = auto_field() 
    rest_pricing = auto_field() 
    rest_image_url = auto_field()
    rest_city = auto_field() 
    rest_state = auto_field() 
    rest_address = auto_field() 
    rest_transactions = auto_field()
    rest_hours = auto_field() 
    rest_website = auto_field() 

city_schema = CitySchema()
event_schema = EventSchema()
restaurant_schema = RestaurantSchema()