from flask import Flask, jsonify
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import create_engine, Column, String, Integer
import os
import json
import requests

def get_events():
    # Get API request
    api_key = "AdXZm2qHWg6t5LVPf6Yyv4abvyhG1hgM"
    search_url = "https://app.ticketmaster.com/discovery/v2/events.json?"
    search_url += 'apikey=' + api_key
    cities_list = []
    with open(os.path.join(os.getcwd(), "cities.txt")) as f:
        cities_list = [x.rstrip('\n') for x in f.readlines() ]
    events_data = {}
    for curr_city in cities_list:
        response = requests.get(search_url + "&city=" + str(curr_city) + "&size=1" + "&countryCode=US")
        try:
            events_data.update({response.json()['_embedded']['events'][0]['id']:response.json()['_embedded']['events'][0]})
        except:
            print("could not find event data for " + str(curr_city))
    file_name = os.path.join(os.getcwd(), 'data/all_events_updated.json')
    with open(file_name, 'w') as f:
        json.dump(events_data, f, indent = 4)

# def go_through_json():
#     file_path = os.path.join(os.getcwd(), 'test.json')
#     file = open(file_path, 'r')
#     events = json.load(file)
#     events_list = []
#     for item in events:
#         print(events[item]['name'])

if __name__ == "__main__":
    get_events()
