import json
import requests
import os

def main():
    subscriptionKey = ""
    endpoint = "https://api.bing.microsoft.com/v7.0/images/search"

    cities_list = []
    with open(os.path.join(os.getcwd(), "cities.txt")) as f:
        cities_list = [x.rstrip('\n') for x in f.readlines() ]
    image_list = []
    for curr_city in cities_list:
        # ex. if you want to search city skyline, searchTerm = "city+skyline"
        searchTerm = curr_city

        # Add your Bing Custom Search endpoint to your environment variables.
        url = endpoint + "?q=" + searchTerm

        r = requests.get(url, headers={'Ocp-Apim-Subscription-Key': subscriptionKey})
        print(r.json()["value"][0]["contentUrl"])
        try:
            d = r.json()["value"][0]["contentUrl"] # gets first result 
            image_list.append(d)
        except:
            pass
    
    with open('cityImages.txt', 'w') as filehandle:
        json.dump(image_list, filehandle)
main()