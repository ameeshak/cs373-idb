from sqlalchemy import MetaData, Table, create_engine  
from sqlalchemy import Column, String, Integer, Float, ARRAY, JSON  
from sqlalchemy.ext.declarative import declarative_base  
from sqlalchemy.orm import sessionmaker
import json
import os
from init import db

db_string = ''

# db = create_engine(db_string)
metadata = MetaData(bind=db)
base = declarative_base()

class City(base):
    __tablename__ = 'Cities'
    city_id = Column(String, primary_key=True)
    city_name = Column(String)
    city_population = Column(Integer)
    city_walk_score_url = Column(String)
    city_budget = Column(Integer)
    city_safety = Column(Integer)
    city_covid = Column(Float)
    city_avg_rating = Column(Float)
    city_known_for = Column(ARRAY(JSON))
    city_image = Column(String)
    city_state = Column(String)

    def __init__(self, city_id="NaN",city_name="NaN",city_population=0,city_walk_score_url="NaN",city_budget=0,city_safety=0,city_covid=0.0,city_avg_rating=0.0,city_known_for=[], city_image="NaN", city_state="NaN"):
        self.city_id = city_id
        self.city_name = city_name
        self.city_population = city_population
        self.city_walk_score_url = city_walk_score_url
        self.city_budget = city_budget
        self.city_safety = city_safety
        self.city_covid = city_covid
        self.city_avg_rating = city_avg_rating
        self.city_known_for = city_known_for
        self.city_image = city_image
        self.city_state = city_state


def create_cities_table():
    base.metadata.create_all(db)

def get_image_data():
    myfile = open('cityImages2.txt', 'r')
    data = myfile.read().strip()
    data = data[1:len(data)-1]
    splitted = data.split(", ")
    return splitted

def populate_cities_table():
    file_path = os.path.join(os.getcwd(), 'data/CityData2.json')
    cities_file = open(file_path, 'r')
    cities = json.load(cities_file)
    cities_list = []
    images_list = get_image_data()

    for (i, city) in enumerate(cities):
        name = city["data"]["attributes"]["name"]
        long_name = city["data"]["attributes"]["long_name"].split(",")
        budget_lst = list(city["data"]["attributes"]["budget"])
        safety_lst = list(city["data"]["attributes"]["safety"])
        covid_lst = list(city["data"]["attributes"]["covid"])
        image_url = images_list[i]

        curr_city = City(
            city_id = city["data"]["id"],
            city_name = name[0 : len(name) - 4],
            city_population = city["data"]["attributes"]["population"],
            city_walk_score_url = city["data"]["attributes"]["walk_score_url"],
            city_budget = city["data"]["attributes"]["budget"][budget_lst[0]]["value"] if len(budget_lst) != 0 else 0,
            city_safety = city["data"]["attributes"]["safety"][safety_lst[0]]["value"] if len(safety_lst) != 0 else 0,
            city_covid = city["data"]["attributes"]["covid"][covid_lst[0]]["value"] if len(covid_lst) != 0 else 0.0,
            city_avg_rating = city["data"]["attributes"]["average_rating"],
            city_known_for = city["data"]["relationships"]["known_for"]["data"],
            city_image = image_url[1:-1],
            city_state = long_name[-2]
        )
        cities_list.append(curr_city)
    
    cities_file.close()
    return cities_list

    # Add to database
def upload(list):
    Session = sessionmaker(db)  
    session = Session()
    session.add_all(list)
    session.commit()

def add_rows(list):
    cities_table = Table('Cities', metadata, autoload = True)
    Session = sessionmaker(db)
    session = Session()
    session.add_all(list)
    session.commit()


    