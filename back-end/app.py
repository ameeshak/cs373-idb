import os

from schemas import city_schema, event_schema, restaurant_schema
from cities import City
from restaurants import Restaurant
from events import Events
from init import db, app
from flask import request, jsonify
from sqlalchemy import or_, cast, desc, func
import sqlalchemy.sql.sqltypes as types

city_filters = ["state", "known_for"]
city_range =  ["covid", "budget", "safety", "population", "avg_rating"]
event_range = ["time", "date", "price"]
event_filters = ["state", "type"]
rest_filters = ["rating", "pricing", "type", "state"]
rest_range = ["review_count"]
event_search = ["name", "type", "venue", "date", "time", "city", "state", "address", "price"]
rest_search = ["name", "type", "city", "rating", "review_count", "pricing", "state", "transactions", "address", "hours"]
city_search = ["name", "state", "avg_rating", "budget", "population", "covid", "safety"]

@app.route('/')
def hello_world():
    return 'Out n About Backend v3'

@app.route('/api/search')
def search():
    query = request.args.get("query")
    searchCities = db.session.query(City).filter(or_(
        City.city_name.ilike('%'+query+'%'),
        City.city_state.ilike('%'+query+'%')
    ))
    cityResult = city_schema.dump(searchCities, many=True)

    searchRests = db.session.query(Restaurant).filter(or_(
        Restaurant.rest_name.ilike('%'+query+'%'),
        Restaurant.rest_city.ilike('%'+query+'%'),
        Restaurant.rest_state.ilike('%'+query+'%'),
        Restaurant.rest_type.ilike('%'+query+'%')
    ))
    restResult = restaurant_schema.dump(searchRests, many=True)

    searchEvents = db.session.query(Events).filter(or_(
        Events.event_name.ilike('%'+query+'%'),
        Events.event_city.ilike('%'+query+'%'),
        Events.event_state.ilike('%'+query+'%'),
        Events.event_venue.ilike('%'+query+'%'),
        Events.event_type.ilike('%'+query+'%')
    ))
    eventResult = event_schema.dump(searchEvents, many=True)

    result = {
        'Cities': cityResult,
        'Restaurants': restResult,
        'Events': eventResult
    }
    return jsonify(result)

@app.route('/api/cities')
def get_cities():
    all_cities = db.session.query(City)
    city_args = request.args.to_dict()
    sort = request.args.get("sort")
    search = request.args.get("search")
    page = request.args.get('page', 1, type=int)

    # filtering
    for curr_filter in city_filters:
        if curr_filter in city_args:
            attr = cast(getattr(City, ("city_" + curr_filter)), types.String)
            values = []
            for val in city_args[curr_filter].split(","):
                values.append(attr.ilike(f"%{val}%"))
            all_cities = all_cities.filter(or_(*values))
    for curr_filter in city_range:
        if curr_filter in city_args:
            attr = cast(getattr(City, ("city_" + curr_filter)), types.Float)
            values = []
            min, max = city_args[curr_filter].split('-')
            all_cities = all_cities.filter(attr.between(min, max))
    # sorting
    if sort != None:
        if request.args.get("desc") != None:
            all_cities = all_cities.order_by((getattr(City, ("city_" + sort))).desc())
        else:
            all_cities = all_cities.order_by(getattr(City, ("city_" + sort)))
    
    # searching
    if search != None:
        search_values = []
        for col in city_search:
            for curr_search in search.split(" "):
                w = cast(getattr(City, ("city_" + col)), types.String).ilike(
                    f"%{curr_search}%")
                search_values.append(w)
        all_cities = all_cities.filter(or_(*search_values))
    result = city_schema.dump(all_cities, many=True)
    per_page = request.args.get("per_page", 9, type=int)
    firstPageIndex = (page - 1) * per_page
    lastPageIndex = firstPageIndex + per_page
    data_page = None
    if (result):
        data_pag = result[firstPageIndex : lastPageIndex]
    return jsonify({"data": result, "paginate": data_pag})


@app.route('/api/cities/id=<string:id>')
def get_city_by_id(id):
    city = db.session.query(City).filter_by(city_id=id).first()
    result = city_schema.dumps(city)
    return jsonify(result)

@app.route('/api/restaurants')
def get_restaurants():
    all_restaurants = db.session.query(Restaurant)
    rest_args = request.args.to_dict()
    sort = request.args.get("sort")
    search = request.args.get("search")
    page = request.args.get('page', 1, type=int)

    # filtering
    for curr_filter in rest_filters:
        if curr_filter in rest_args:
            attr = cast(getattr(Restaurant, ("rest_" + curr_filter)), types.String)
            values = []
            for val in rest_args[curr_filter].split(","):
                values.append(attr.ilike(val)) 
            all_restaurants = all_restaurants.filter(or_(*values))
    for curr_filt in rest_range:
        if curr_filt in rest_args:
            attr = cast(getattr(Restaurant, ("rest_" + curr_filt)), types.Integer)
            values = []
            min, max = rest_args[curr_filt].split('-')
            all_restaurants = all_restaurants.filter(attr.between(min, max))


    # sorting
    if sort != None:
        if request.args.get("desc") != None:
            all_restaurants = all_restaurants.order_by((getattr(Restaurant, ("rest_" + sort))).desc())
        else:
            all_restaurants = all_restaurants.order_by(getattr(Restaurant, ("rest_" + sort)))
    # searching
    if search != None:
        search_values = []
        for col in rest_search:
            for curr_search in search.split(" "):
                w = cast(getattr(Restaurant, ("rest_" + col)), types.String).ilike(
                    f"%{curr_search}%")
                search_values.append(w)
        all_restaurants = all_restaurants.filter(or_(*search_values))
    result = restaurant_schema.dump(all_restaurants, many=True)
    per_page = request.args.get("per_page", 9, type=int)
    firstPageIndex = (page - 1) * per_page
    lastPageIndex = firstPageIndex + per_page
    data_page = None
    if (result):
        data_pag = result[firstPageIndex : lastPageIndex]
    return jsonify({"data": result, "paginate": data_pag})


@app.route('/api/restaurants/id=<string:id>')
def get_restaurant_by_id(id):
    restaurant = db.session.query(Restaurant).filter_by(rest_id=id).first()
    result = restaurant_schema.dumps(restaurant)
    return jsonify(result)

@app.route('/api/events')
def get_events():
    all_events = db.session.query(Events)
    event_args = request.args.to_dict()
    sort = request.args.get("sort")
    search = request.args.get("search")
    page = request.args.get('page', 1, type=int)
    # filtering
    for curr_filter in event_filters:
        if curr_filter in event_args:
            attr = cast(getattr(Events, ("event_" + curr_filter)), types.String)
            values = []
            for val in event_args[curr_filter].split(","):
                values.append(attr.ilike(f"%{val}%"))
            all_events = all_events.filter(or_(*values))
    for curr_filt in event_range:
        if curr_filt in event_args:
            if (curr_filt == 'price'):
                attr = cast(getattr(Events, ("event_" + curr_filt)), types.Float)
                values = []
                min, max = event_args[curr_filt].split('-')
                all_events = all_events.filter(attr.between(min, max))
            if (curr_filt == 'time'):
                attr = cast(getattr(Events, ("event_" + curr_filt)), types.String)
                values = []
                min, max = event_args[curr_filt].split('-')
                all_events = all_events.filter(attr.between(min, max))
            if (curr_filt == 'date'):
                attr = cast(getattr(Events, ("event_" + curr_filt)), types.String)
                values = []
                min, max = event_args[curr_filt].split(':')
                all_events = all_events.filter(attr.between(min, max))

    # sorting
    if sort != None:
        if request.args.get("desc") != None:
            all_events = all_events.order_by((getattr(Events, ("event_" + sort))).desc())
        else:
            all_events = all_events.order_by(getattr(Events, ("event_" + sort)))
    # searching
    if search != None:
        search_values = []
        for col in event_search:
            for curr_search in search.split(" "):
                w = cast(getattr(Events, ("event_" + col)), types.String).ilike(
                    f"%{curr_search}%")
                search_values.append(w)
        all_events = all_events.filter(or_(*search_values))

    result = event_schema.dump(all_events, many=True)
    per_page = request.args.get("per_page", 9, type=int)
    firstPageIndex = (page - 1) * per_page
    lastPageIndex = firstPageIndex + per_page
    data_page = None
    if (result):
        data_pag = result[firstPageIndex : lastPageIndex]
    return jsonify({"data": result, "paginate": data_pag})

@app.route('/api/events/id=<string:id>')
def get_event_by_id(id):
    event = db.session.query(Events).filter_by(event_id=id).first()
    result = event_schema.dumps(event)
    return jsonify(result)

if __name__ == "__main__":
    from waitress import serve
    serve(app, host="0.0.0.0", port=5000)