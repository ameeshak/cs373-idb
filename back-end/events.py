from sqlalchemy import create_engine  
from sqlalchemy import Column, String, Float
from sqlalchemy.ext.declarative import declarative_base  
from sqlalchemy.orm import sessionmaker
import json
import os
from init import db


# db_string = 'topsecret'
# db = create_engine(db_string)

base = declarative_base()


# Define Events table
class Events(base):
    __tablename__ = 'Events'
    event_id = Column(String, primary_key=True)
    event_name = Column(String)
    event_city = Column(String)
    event_venue = Column(String)
    event_type = Column(String)
    event_time = Column(String)
    event_date= Column(String)
    event_state = Column(String)
    event_address = Column(String)
    event_url = Column(String)
    event_image = Column(String)
    event_price = Column(Float)
    event_accessibility = Column(String)
    event_timezone = Column(String)


def __init__(self, event_id="NaN", event_name="NaN", event_city="NaN", event_venue="NaN", event_type="NaN", event_time="NaN", event_date="NaN", event_state="NaN", event_address="NaN", event_url = "NaN", event_image = "NaNs",  event_price=0.0, event_accessibility="NaN"):
    self.event_id = event_id
    self.event_name = event_name
    self.event_city = event_city
    self.event_venue = event_venue
    self.event_type = event_type
    self.event_time = event_time
    self.event_date = event_date
    self.event_state = event_state
    self.event_address = event_address
    self.event_url = event_url
    self.event_image = event_image
    self.event_price = event_price
    self.event_accessibility = event_accessibility

def create_events_table():
    base.metadata.create_all(db)

def events_to_db():
    file_path = os.path.join(os.getcwd(), 'data/all_events_updated.json')
    file = open(file_path, 'r')
    events = json.load(file)
    events_list = []
    for item in events:
        curr_event = Events(
            event_id= events[item]['id'],
            event_name= events[item]['name'],
            event_city= events[item]['_embedded']['venues'][0]['city']['name'],
            event_venue= events[item]['_embedded']['venues'][0]['name'],
            event_type= events[item]['classifications'][0]['segment']['name'] if 'genre' in list(events[item]['classifications'][0]) else 'N/A',
            event_time= events[item]['dates']['start']['localTime'] if 'localTime' in list(events[item]['dates']['start']) else 'N/A',
            event_date = events[item]['dates']['start']['localDate'],
            event_state= events[item]['_embedded']['venues'][0]['state']['name'] if 'state' in list(events[item]['_embedded']['venues'][0]) else 'N/A',
            event_address= events[item]['_embedded']['venues'][0]['address']['line1'] if 'line1' in list(events[item]['_embedded']['venues'][0]['address']) else 'N/A',
            event_url= events[item]['url'],
            event_image = events[item]['images'][0]['url'],
            event_price = json.dumps(events[item]['priceRanges'][0]['min']) if 'priceRanges' in list(events[item]) else 0.0,
            event_accessibility = events[item]['_embedded']['venues'][0]['accessibleSeatingDetail'] if 'accessibleSeatingDetail' in  list(events[item]['_embedded']['venues'][0]) else []
            )
        events_list.append(curr_event)
    file.close()
    return events_list

# Adds entries to the database
def upload(list):
    Session = sessionmaker(db)  
    session = Session()
    session.add_all(list)
    session.commit()
