import requests
import json

city_api_user=''
city_api_pass=''

city_data = []
def city_data_json():
    index = 0
    with open('back-end/cities2.txt') as city_file:
        for line in city_file:
            url = 'https://api.roadgoat.com/api/v2/destinations/' + line.replace('\n', '')
            data = requests.get(url=url, auth=(city_api_user, city_api_pass))
            data = data.json()
            city_data.append(data)
            index += 1
            print(index)

    with open('CityData2.json', 'w') as outfile:
        json.dump(city_data, outfile)

if __name__ == "__main__":
    city_data_json()
