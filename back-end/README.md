# How to Run
1. `cd back-end`  
2. `docker build --tag back-end .`  
3. `docker run -p 5000:5000 back-end`  
4. Visit `localhost:5000` in your favorite browser  🎉


